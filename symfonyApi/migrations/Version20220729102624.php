<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220729102624 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE mahmoud (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, age VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE code_projet CHANGE budget_nrjconsomme budget_nrjconsomme VARCHAR(500) DEFAULT \'0\' NOT NULL, CHANGE budget_decoconsomme budget_decoconsomme VARCHAR(500) DEFAULT \'0\' NOT NULL, CHANGE budget_cloeconsomme budget_cloeconsomme VARCHAR(500) DEFAULT \'0\' NOT NULL, CHANGE charge_nrjconsomme charge_nrjconsomme VARCHAR(500) DEFAULT \'0\' NOT NULL, CHANGE charge_decoconsomme charge_decoconsomme VARCHAR(500) DEFAULT \'0\' NOT NULL, CHANGE charge_cloeconsomme charge_cloeconsomme VARCHAR(255) DEFAULT \'0\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE mahmoud');
        $this->addSql('ALTER TABLE code_projet CHANGE budget_nrjconsomme budget_nrjconsomme VARCHAR(500) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE budget_decoconsomme budget_decoconsomme VARCHAR(500) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE budget_cloeconsomme budget_cloeconsomme VARCHAR(500) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE charge_nrjconsomme charge_nrjconsomme VARCHAR(500) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE charge_decoconsomme charge_decoconsomme VARCHAR(500) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE charge_cloeconsomme charge_cloeconsomme VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
