<?php

namespace App\Repository;

use App\Entity\DetailsImputation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DetailsImputation|null find($id, $lockMode = null, $lockVersion = null)
 * @method DetailsImputation|null findOneBy(array $criteria, array $orderBy = null)
 * @method DetailsImputation[]    findAll()
 * @method DetailsImputation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DetailsImputationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DetailsImputation::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(DetailsImputation $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(DetailsImputation $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @return DetailsImputation[] Returns an array of DetailsImputation objects
     */

    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.date = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?DetailsImputation
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
