<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ImputationRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;


/**
 * @ORM\Entity(repositoryClass=ImputationRepository::class)
 * @ApiResource
 * @ApiFilter(SearchFilter::class)
 * 
 * 
    
 */
class Imputation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $commentaire;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="imputations")
     * @ORM\JoinColumn(nullable=false)
     * @ApiFilter(SearchFilter::class)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=DetailsImputation::class, mappedBy="imputation")
     */
    private $detailsImputations;

    public function __construct()
    {
        $this->detailsImputations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, DetailsImputation>
     */
    public function getDetailsImputations(): Collection
    {
        return $this->detailsImputations;
    }

    public function addDetailsImputation(DetailsImputation $detailsImputation): self
    {
        if (!$this->detailsImputations->contains($detailsImputation)) {
            $this->detailsImputations[] = $detailsImputation;
            $detailsImputation->setImputation($this);
        }

        return $this;
    }

    public function removeDetailsImputation(DetailsImputation $detailsImputation): self
    {
        if ($this->detailsImputations->removeElement($detailsImputation)) {
            // set the owning side to null (unless already changed)
            if ($detailsImputation->getImputation() === $this) {
                $detailsImputation->setImputation(null);
            }
        }

        return $this;
    }
}
