<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\CodeProjetRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=CodeProjetRepository::class)
 * @ApiResource
 */
class CodeProjet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="date")
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="date")
     */
    private $dateFin;

    /**
     * @ORM\Column(type="boolean")
     */
    private $statut;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $budget;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $budgetNRJ;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $budgetDECO;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $budgetCLOE;

    /**
     * @ORM\Column(type="string", length=500,options={"default" : 0})
     */
    private $budgetNRJconsomme;

    /**
     * @ORM\Column(type="string", length=500,options={"default" : 0})
     */
    private $budgetDECOconsomme;

    /**
     * @ORM\Column(type="string", length=500,options={"default" : 0})
     */
    private $budgetCLOEconsomme;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $charge;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $chargeNRJ;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $chargeDECO;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $chargeCLOE;

    /**
     * @ORM\Column(type="string", length=500,options={"default" : 0})
     */
    private $chargeNRJconsomme;

    /**
     * @ORM\Column(type="string", length=500,options={"default" : 0})
     */
    private $chargeDECOconsomme;

    /**
     * @ORM\Column(type="string", length=255,options={"default" : 0})
     */
    private $chargeCLOEconsomme;

    /**
     * @ORM\OneToMany(targetEntity=DetailsImputation::class, mappedBy="codeprojet")
     */
    private $detailsImputations;

    /**
     * @ORM\OneToMany(targetEntity=Tache::class, mappedBy="codeprojet")
     */
    private $taches;

    public function __construct()
    {
        $this->detailsImputations = new ArrayCollection();
        $this->taches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getStatut(): ?bool
    {
        return $this->statut;
    }

    public function setStatut(bool $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getBudget(): ?string
    {
        return $this->budget;
    }

    public function setBudget(string $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getBudgetNRJ(): ?string
    {
        return $this->budgetNRJ;
    }

    public function setBudgetNRJ(string $budgetNRJ): self
    {
        $this->budgetNRJ = $budgetNRJ;

        return $this;
    }

    public function getBudgetDECO(): ?string
    {
        return $this->budgetDECO;
    }

    public function setBudgetDECO(string $budgetDECO): self
    {
        $this->budgetDECO = $budgetDECO;

        return $this;
    }

    public function getBudgetCLOE(): ?string
    {
        return $this->budgetCLOE;
    }

    public function setBudgetCLOE(string $budgetCLOE): self
    {
        $this->budgetCLOE = $budgetCLOE;

        return $this;
    }

    public function getBudgetNRJconsomme(): ?string
    {
        return $this->budgetNRJconsomme;
    }

    public function setBudgetNRJconsomme(string $budgetNRJconsomme): self
    {
        $this->budgetNRJconsomme = $budgetNRJconsomme;

        return $this;
    }

    public function getBudgetDECOconsomme(): ?string
    {
        return $this->budgetDECOconsomme;
    }

    public function setBudgetDECOconsomme(string $budgetDECOconsomme): self
    {
        $this->budgetDECOconsomme = $budgetDECOconsomme;

        return $this;
    }

    public function getBudgetCLOEconsomme(): ?string
    {
        return $this->budgetCLOEconsomme;
    }

    public function setBudgetCLOEconsomme(string $budgetCLOEconsomme): self
    {
        $this->budgetCLOEconsomme = $budgetCLOEconsomme;

        return $this;
    }

    public function getCharge(): ?string
    {
        return $this->charge;
    }

    public function setCharge(string $charge): self
    {
        $this->charge = $charge;

        return $this;
    }

    public function getChargeNRJ(): ?string
    {
        return $this->chargeNRJ;
    }

    public function setChargeNRJ(string $chargeNRJ): self
    {
        $this->chargeNRJ = $chargeNRJ;

        return $this;
    }

    public function getChargeDECO(): ?string
    {
        return $this->chargeDECO;
    }

    public function setChargeDECO(string $chargeDECO): self
    {
        $this->chargeDECO = $chargeDECO;

        return $this;
    }

    public function getChargeCLOE(): ?string
    {
        return $this->chargeCLOE;
    }

    public function setChargeCLOE(string $chargeCLOE): self
    {
        $this->chargeCLOE = $chargeCLOE;

        return $this;
    }

    public function getChargeNRJconsomme(): ?string
    {
        return $this->chargeNRJconsomme;
    }

    public function setChargeNRJconsomme(string $chargeNRJconsomme): self
    {
        $this->chargeNRJconsomme = $chargeNRJconsomme;

        return $this;
    }

    public function getChargeDECOconsomme(): ?string
    {
        return $this->chargeDECOconsomme;
    }

    public function setChargeDECOconsomme(string $chargeDECOconsomme): self
    {
        $this->chargeDECOconsomme = $chargeDECOconsomme;

        return $this;
    }

    public function getChargeCLOEconsomme(): ?string
    {
        return $this->chargeCLOEconsomme;
    }

    public function setChargeCLOEconsomme(string $chargeCLOEconsomme): self
    {
        $this->chargeCLOEconsomme = $chargeCLOEconsomme;

        return $this;
    }

    /**
     * @return Collection<int, DetailsImputation>
     */
    public function getDetailsImputations(): Collection
    {
        return $this->detailsImputations;
    }

    public function addDetailsImputation(DetailsImputation $detailsImputation): self
    {
        if (!$this->detailsImputations->contains($detailsImputation)) {
            $this->detailsImputations[] = $detailsImputation;
            $detailsImputation->setCodeprojet($this);
        }

        return $this;
    }

    public function removeDetailsImputation(DetailsImputation $detailsImputation): self
    {
        if ($this->detailsImputations->removeElement($detailsImputation)) {
            // set the owning side to null (unless already changed)
            if ($detailsImputation->getCodeprojet() === $this) {
                $detailsImputation->setCodeprojet(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Tache>
     */
    public function getTaches(): Collection
    {
        return $this->taches;
    }

    public function addTach(Tache $tach): self
    {
        if (!$this->taches->contains($tach)) {
            $this->taches[] = $tach;
            $tach->setCodeprojet($this);
        }

        return $this;
    }

    public function removeTach(Tache $tach): self
    {
        if ($this->taches->removeElement($tach)) {
            // set the owning side to null (unless already changed)
            if ($tach->getCodeprojet() === $this) {
                $tach->setCodeprojet(null);
            }
        }

        return $this;
    }
}
