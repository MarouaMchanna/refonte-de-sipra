<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\TacheRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=TacheRepository::class)
 * @ApiResource
 */
class Tache
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $domaine;

    /**
     * @ORM\OneToMany(targetEntity=DetailsImputation::class, mappedBy="tache")
     */
    private $detailsImputations;

    /**
     * @ORM\ManyToOne(targetEntity=CodeProjet::class, inversedBy="taches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $codeprojet;

    public function __construct()
    {
        $this->detailsImputations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDomaine(): ?string
    {
        return $this->domaine;
    }

    public function setDomaine(string $domaine): self
    {
        $this->domaine = $domaine;

        return $this;
    }

    /**
     * @return Collection<int, DetailsImputation>
     */
    public function getDetailsImputations(): Collection
    {
        return $this->detailsImputations;
    }

    public function addDetailsImputation(DetailsImputation $detailsImputation): self
    {
        if (!$this->detailsImputations->contains($detailsImputation)) {
            $this->detailsImputations[] = $detailsImputation;
            $detailsImputation->setTache($this);
        }

        return $this;
    }

    public function removeDetailsImputation(DetailsImputation $detailsImputation): self
    {
        if ($this->detailsImputations->removeElement($detailsImputation)) {
            // set the owning side to null (unless already changed)
            if ($detailsImputation->getTache() === $this) {
                $detailsImputation->setTache(null);
            }
        }

        return $this;
    }

    public function getCodeprojet(): ?CodeProjet
    {
        return $this->codeprojet;
    }

    public function setCodeprojet(?CodeProjet $codeprojet): self
    {
        $this->codeprojet = $codeprojet;

        return $this;
    }
}
