<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DetailsImputationRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ORM\Entity(repositoryClass=DetailsImputationRepository::class)
 * @ApiResource
 * 
 * @ApiFilter(SearchFilter::class)
 */
class DetailsImputation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $valeur;

    /**
     * @ORM\ManyToOne(targetEntity=Imputation::class, inversedBy="detailsImputations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $imputation;

    /**
     * @ORM\ManyToOne(targetEntity=CodeProjet::class, inversedBy="detailsImputations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $codeprojet;

    /**
     * @ORM\ManyToOne(targetEntity=Tache::class, inversedBy="detailsImputations")
     */
    private $tache;

    /**
     * @ORM\ManyToOne(targetEntity=Activite::class, inversedBy="detailsImputations")
     */
    private $activite;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getValeur(): ?string
    {
        return $this->valeur;
    }

    public function setValeur(string $valeur): self
    {
        $this->valeur = $valeur;

        return $this;
    }

    public function getImputation(): ?Imputation
    {
        return $this->imputation;
    }

    public function setImputation(?Imputation $imputation): self
    {
        $this->imputation = $imputation;

        return $this;
    }

    public function getCodeprojet(): ?CodeProjet
    {
        return $this->codeprojet;
    }

    public function setCodeprojet(?CodeProjet $codeprojet): self
    {
        $this->codeprojet = $codeprojet;

        return $this;
    }

    public function getTache(): ?Tache
    {
        return $this->tache;
    }

    public function setTache(?Tache $tache): self
    {
        $this->tache = $tache;

        return $this;
    }

    public function getActivite(): ?Activite
    {
        return $this->activite;
    }

    public function setActivite(?Activite $activite): self
    {
        $this->activite = $activite;

        return $this;
    }
}
