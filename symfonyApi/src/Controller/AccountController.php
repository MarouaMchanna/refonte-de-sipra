<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\ImputationRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Constraints\Email;


class AccountController extends AbstractController
{


    /**
     * @Route("/api/login_check", name="login_account", methods={"POST"})
     */
    public function login(): Response
    {
        return $this->json([
            'method' => 'login',
        ]);
    }

    /**
     * @Route("/logout", name="app_logout", methods={"GET"})
     */
    public function logout()
    {
        throw new \Exception('should not be reached');
    }

    /**
     * Decode the Token and get his username and roles
     * 
     * @Route("api/decoderToken", name="decode_user", methods={"POST"})
     */
    public function decoderToken(Request $request): Response
    {
        $tokenParts = explode(".", $request->getContent());
        $tokenHeader = base64_decode($tokenParts[0]);
        $tokenPayload = base64_decode($tokenParts[1]);
        $jwtHeader = json_decode($tokenHeader);
        $jwtPayload = json_decode($tokenPayload);
        return $this->json([
            'username' => $jwtPayload->username,
            'roles' => $jwtPayload->roles,
        ]);
    }



    /**
     * Find user by email
     *
     * @param UserRepository $repo
     * @param [type] $email
     * @Route("api/UserLoged", name="login_user", methods={"POST"})
     */
    public function UserLoged(UserRepository $repo, $email)
    {
        $user = $repo->findOneByEmail(array('email' => $email));
        return $this->json([
            'user' => $user
        ]);
    }

    /**
     * Find imputation by user
     * 
     * @param  ImputationRepository $repo 
     * @param [type] $id
     * @Route("api/UserImputation", name="user_imputation", methods={"POST"})
     */
    public function UserImputation(ImputationRepository $repo, int $id)
    {
        $userImput = $repo->findByUser($id);
        return $this->json([
            'ImputationUser' => $userImput
        ]);
    }


    /**
     *  
     * @Route("/api/hach", name="hach_password", methods={"GET"})
     */
    public function hachPassword(UserPasswordHasherInterface $passwordHasher): Response
    {
        $user = new User;
        //return bcrypt.hashSync($password, 8);
        $password = "000000";
        // hash the password (based on the security.yaml config for the $user class)
        $hashedPassword = $passwordHasher->hashPassword(
            $user,
            $password
        );
        return $this->json([
            'hashed password' => $hashedPassword
        ]);
    }
    /**
     * @Route("/api/saveImage", name="save_image", methods={"POST"})
     */
    public function save(Request $request): Response
    {
        $folderPath = "uploads/images/users/";
        $requestData = json_decode($request->getContent(), true);
        $image_parts = explode(";base64,", $requestData['fileSource']);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file = $folderPath . $requestData['nom_image'] . '.png';
        file_put_contents($file, $image_base64);
        return $this->json([
            'method' => 'save',
            'fileSource' => $requestData['fileSource'],
        ]);
    }
}
