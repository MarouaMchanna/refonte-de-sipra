<?php

namespace App\Controller;

use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MeController extends AbstractController
{
    private $security;
    
    public function __construct(Security $security){
        $this->security = $security;
    }

    /**
     * @Route("api/me", name="me_controller", methods={"GET"})
     */
    public function __invoke()
    {
        return $this->security->getUser();;
    }
}
