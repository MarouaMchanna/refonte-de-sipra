# Refonte de SIPRA

## Description

C'est une application de suivi financier et de getion d'imputations :

  - Pour tous type d'utilisateurs : 
       Apres l'acces a notre site, la premiere etape c'est l'authentification. Un visiteur n'as pas le droit d'acceder a quelconque action ou information sur le site.
       chaque utilisateur a un email et un mot de passe generer par son Admin, qui leur permis de se loger a notre systeme.
       Si l'authentification est reussite, l'utilisateur serra directement rediriger vers le Layout correspondant a son role; si c'est un Admin, il serra rediriger vers les interfaces Admin, sinon il serra rediriger vers les interfaces User;  

  - Admin Interfaces: 
     
       * Ajouter/Modifier/Supprimer : 
           - Les collaborateurs 
           - Les projets
           - Les imputations
           - Les taches 
           - Les activitees
       * Les imputations :
           - rechercher les imputations par semaine de n'importe quel utilisateurs 
           - Les exporter en fichier Exel par semaine/ ou par mois   
       * Visualiser l'accueil :
           - des statistiques qui peut aider a la prise de decision.
       * Visualiser/Modifier son profil :
           - Afficher les informations de l'Admin connecter
           - Modifier les informations personnel
           - Modifier son mot de passe
       * Se deconnecter :

  - User Interfaces:
       * Les imputations :
           - rechercher les imputations par semaine de cet utilisateur
           - les Ajouter/Modifier/Supprimer 
       * Visualiser l'accueil :
           - des statistiques qui peut aider a la prise de decision.
       * Visualiser/Modifier son profil :
           - Afficher les informations du User connecter
           - Modifier les informations personnel
           - Modifier son mot de passe 
       * Se deconnecter :    
              
## Getting started  

Avant de commencer, il faut comprendre que :
-  App-Front c'est le dossier qui contient toute la partie presentation de l'application developpee en utilisant le Framwork Angular.
-  SymfonyAPI c'est le dossier qui contient nos web services graphQL developees en utilisant le Framwork PHP Symfony 6, on a profite d'utiliser aussi API platform, qui est une distribution Symfony, afin d'optimiser et d'automatiser le processus de developpement d'Api et de leurs documentation.

* Les etapes pour commencer: 

  - cloner le repertoire sur votre machine:
    il faut creer un dossier sur votre machine, l'ouvrir en git Bach, taper la commande suivante:
       [[ git clone https://gitlab.com/stage-pfe3/refonte-de-sipra.git  ]] 
    Apres, le repertoire et ses dossiers doivent apparaitre sur votre machine au sien du dossier creer au debut. 


   - Pour les web services Symfony :
      * Préparation de l’installation de l’application :

            1- Avant toute chose, il faut disposer au minimum d’un serveur web ( Apache ou Nginx) et d’un PHP (version 8.0 ou plus)

            2- la mise en place du gestionnaire de dépendances Composer est indispensable, voici le lien https://getcomposer.org/ 

            3- Symfony CLI : pour avoir acces a la commande Symfony

      * Ouvrez le dossier SymfonyAPI avec VSC, ouvrez un nouveau terminal et executer les commandes   suivantes:

             1- " composer install " ----> pour installer toute dependances que mon projet va en avoir besoin

             2- " symfony server:start " ---> pour démarrer le serveur de développement et executer nos web services 

             3- Si vous pocediez pas de commande symfony, vous pouvez utiliser la commande suivante :  " php -S localhost:8000 -t public/ "

             4- il faut faire la liaison avec la base de donnee, en excecutant ces commandes:
               " php bin/console doctrine:database:create " ----> pour creer la BD 
               "php bin/console doctrine:migrations:migrate" ----> pour executer les migrations

   - Pour la partie Front:
       * Avant d'utiliser Angular il faut installer un certain nombre de logiciels :

            - Node.js : Impossible de faire fonctionner Angular sans lui.
            - Visual Studio Code : Ce choix est arbitraire.
            - Git : Très utile mais pas essentiel
            - Angular CLI : C'est l'homme à tout faire d'Angular


       * Ouvrez le dossier App-Front avec VSC, ouvrez un nouveau terminal et executer les commandes suivantes:

             1- " npm install " ----> pour installer toute dependances que mon projet va en avoir besoin
             2- " ng serve  " ---> pour lancer et executer notre application

        
## Authors and acknowledgment
Ce projet a ete realiser par Maroua M'channa et Lamiae El Hammoumi.

## License
For open source projects, say how it is licensed.

## Project status

