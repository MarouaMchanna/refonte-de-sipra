import { Component, OnInit, OnDestroy, Output } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthServiceService } from '../services/auth-service/auth-service.service';
import { Injectable } from '@angular/core';
import swal from 'sweetalert2';
import { CollaborateurService } from '../services/collaborateur-service/collaborateur.service';


@Component({
  selector: 'app-register-cmp',
  templateUrl: './register.component.html'
})

export class RegisterComponent implements OnInit, OnDestroy {
  test: Date = new Date();
  formGroup!: FormGroup;
  collaborateur: any;
  urlImage: any;

  constructor(private authService: AuthServiceService, private collabService: CollaborateurService, private route: Router) { }

  ngOnInit() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('register-page');
    body.classList.add('off-canvas-sidebar');
    this.initForm();
  }
  initForm() {
    this.formGroup = new FormGroup({
      username: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required])
    })
  }
  loginProcess() {
    if (this.formGroup.valid) {

      this.authService.login(this.formGroup.value).subscribe((result) => {
        if (result.error) {
          console.log(result);
          localStorage.setItem("isSigned", "false");
        }

        else {

          localStorage.setItem("token", result.token);
          localStorage.setItem("isSigned", "true");
          console.log(result.token);
          this.authService.GetUserAuth(result.token).subscribe(result1 => {
            if (result1.error) {
              alert("function Getuser doesn't work");
            }
            else {
              console.log(result1);
              if (result1.roles == "ROLE_USER") {
                localStorage.setItem("username", result1.username);
                localStorage.setItem("roles", result1.roles);
                this.collabService.getCollabByE(localStorage.getItem("username")).subscribe((result: any) => {
                  this.collaborateur = result?.data?.users.edges;
                  console.log(this.collaborateur);
                  this.collaborateur.forEach(c => {
                    localStorage.setItem("image", c.node.image);
                    localStorage.setItem("nom", c.node.nom);
                    localStorage.setItem("id", c.node.id);
                    localStorage.setItem("CODE", c.node.codecapit);
                    localStorage.setItem("password", c.node.password);
                  });
                });
                console.log("username");
                console.log("c'est un utilisateur");
                this.route.navigate(['/User']);
              }
              else if (result1.roles.indexOf("ROLE_ADMIN") > -1) {
                localStorage.setItem("username", result1.username);

                this.collabService.getCollabByE(localStorage.getItem("username")).subscribe((result: any) => {
                  this.collaborateur = result?.data?.users.edges;
                  console.log(this.collaborateur);
                  this.collaborateur.forEach(c => {
                    localStorage.setItem("image", c.node.image);
                    localStorage.setItem("nom", c.node.nom);
                    localStorage.setItem("id", c.node.id);
                    localStorage.setItem("CODE", c.node.codecapit);
                    localStorage.setItem("password", c.node.password);
                  });
                });
                localStorage.setItem("roles", result1.roles);
                console.log(localStorage.getItem("username"));
                console.log("c'est un admin");
                this.route.navigate(['/Admin']);
              }

              swal.fire({
                title: "L'authentification a réussi!",
                text: "Bienvenue!",
                buttonsStyling: false,
                customClass: {
                  confirmButton: "btn btn-success",
                },
                icon: "success"
              });
            }
          });


        }
      },
        (error) => {
          console.log(error);
          swal.fire({
            title: "L'authentification a échoué!",
            text: "l'email ou le mot de passe est incorrect",
            buttonsStyling: false,
            customClass: {
              confirmButton: "btn btn-danger"
            }
          });
        });

    }


  }

  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('register-page');
    body.classList.remove('off-canvas-sidebar');
  }
}
