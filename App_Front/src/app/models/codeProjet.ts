export class codeProjet {
    id: number;
    description: string;
    dateDebut: string;
    dateFin: string;
    statut: number;
    budget: number;
    libelle: number;
    budgetNRJ: number;
    budgetDECO: number;
    budgetCLOE: number;
    budgetNRJconsomme: number;
    budgetDECOconsomme: number;
    budgetCLOEconsomme: number;
    charge: number;
    chargeNRJ: number;
    chargeDECO: number;
    chargeCLOE: number;
    chargeNRJconsomme: number;
    chargeDECOconsomme: number;
    chargeCLOEconsomme: number;
}