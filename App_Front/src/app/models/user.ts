import { projet } from "./projet";

export class user {
    id: string;
    projet_id:string;
    projet: projet;
    email: string;
    nom: string;
    roles: string[];
    password: string;
    poste: string;
    CJM: string;
    codecapit: string;
    site: string;
    image: string;

}