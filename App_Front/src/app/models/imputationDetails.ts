import { activite } from "./activite";
import { codeProjet } from "./codeProjet";
import { tache } from "./tache";

export class imputationDetails {
    id: number;
    codeProjet: codeProjet;
    tache: tache;
    activite: activite;
    commentaire: string;
    lun: string;
    mar: string;
    mer: string;
    jeu: string;
    ven: string;
}