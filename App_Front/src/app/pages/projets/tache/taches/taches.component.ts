import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { tache } from 'src/app/models/tache';
import { TacheService } from 'src/app/services/tache-service/tache.service';

declare var $: any;
declare interface TableData {
  headerRow: string[];
}

@Component({
  selector: 'app-taches',
  templateUrl: './taches.component.html',
  styleUrls: ['./taches.component.css']
})
export class TachesComponent implements OnInit, AfterViewInit {
  public tableData1: TableData;
  public tache: tache[];
  constructor(public tachService: TacheService, private route: Router) { }

  ngOnInit() {
    this.tache = this.tachService.getTache();
    this.tableData1 = {
      headerRow: ['Jira', 'Description', 'Code projet', 'Domaine', 'Actions'],
    };
  }

  ngAfterViewInit(): void {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "Tout"]
      ],

      buttons: [
        {
          text: 'My button',
          action: function (e, dt, node, config) {
            alert('Button activated');
          }
        }
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Rechercher",
        "info": "Affichage _START_ à _END_ de _TOTAL_ tâches",
        "lengthMenu": "Afficher _MENU_ tâches",
        "paginate": {
          "first": "Premier",
          "last": "Dernier",
          "next": "Suivant",
          "previous": "Précédent",
        },

      }
    });

  }
  AjouterTache() {
    return this.route.navigate(['Admin/projets/ajout-tache']);
  }

}
