import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { plainToClass } from 'class-transformer';
import { codeProjet } from 'src/app/models/codeProjet';
import { tache } from 'src/app/models/tache';
import { CodeProjetServiceService } from 'src/app/services/code_projet-service/code-projet-service.service';
import { TacheService } from 'src/app/services/tache-service/tache.service';

@Component({
  selector: 'app-ajout-tache',
  templateUrl: './ajout-tache.component.html',
  styleUrls: ['./ajout-tache.component.css']
})
export class AjoutTacheComponent implements OnInit {
  formGroup!: FormGroup;
  codeProjet: codeProjet[];
  tache: tache;
  constructor(private CPService: CodeProjetServiceService, private TService: TacheService, private route: Router) { }
  Codes = [
    { value: 'NRJ', viewValue: 'NRJ' },
    { value: 'DECO', viewValue: 'DECO' },
    { value: 'CLOE', viewValue: 'CLOE' },
    { value: 'NRJ-DECO', viewValue: 'NRJ-DECO' },

  ];
  ngOnInit(): void {
    this.codeProjet = this.CPService.getCodeProjet();
    this.initForm();
  }

  initForm() {
    this.formGroup = new FormGroup({
      libelle: new FormControl("", [Validators.required]),
      description: new FormControl("", [Validators.required]),
      codeprojet_id: new FormControl("", [Validators.required]),
      domaine: new FormControl("", [Validators.required]),
    })
  }

  AjoutTache() {
    if (this.formGroup.valid) {
      console.log(this.formGroup.value);
      this.tache = plainToClass(tache, this.formGroup.value);
      this.TService.AjouterTache(this.tache);
      this.route.navigate(['/Admin/projets/taches']);
    }
  }

}
