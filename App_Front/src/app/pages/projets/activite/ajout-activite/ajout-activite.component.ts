import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { plainToClass } from 'class-transformer';
import { activite } from 'src/app/models/activite';
import { ActiviteService } from 'src/app/services/activite-service/activite.service';

@Component({
  selector: 'app-ajout-activite',
  templateUrl: './ajout-activite.component.html',
  styleUrls: ['./ajout-activite.component.css']
})
export class AjoutActiviteComponent implements OnInit {
  formGroup!: FormGroup;
  activite: activite;
  constructor(public ActService: ActiviteService, public route: Router) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.formGroup = new FormGroup({
      libelle: new FormControl("", [Validators.required]),
      description: new FormControl("", [Validators.required]),
    })
  }

  AjoutActivite() {
    if (this.formGroup.valid) {
      console.log("ajouter un code projet");
      this.activite = plainToClass(activite, this.formGroup.value);
      this.ActService.AjouterActivite(this.activite);
      console.log(this.activite);
      this.route.navigate(['/Admin/projets/activites']);
    }
  }

}
