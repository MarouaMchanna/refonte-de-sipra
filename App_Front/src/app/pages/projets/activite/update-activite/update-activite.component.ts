import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { plainToClass } from 'class-transformer';
import { activite } from 'src/app/models/activite';
import { ActiviteService } from 'src/app/services/activite-service/activite.service';

@Component({
  selector: 'app-update-activite',
  templateUrl: './update-activite.component.html',
  styleUrls: ['./update-activite.component.css']
})
export class UpdateActiviteComponent implements OnInit {
  formGroup!: FormGroup;
  pId: any;
  activite: activite;
  constructor(private routeA: ActivatedRoute, private actService: ActiviteService, private route: Router) { }

  ngOnInit(): void {
    //recuperer l'id de l'utilisateur selectionne 
    this.routeA.queryParamMap
      .subscribe((params) => {
        this.pId = params.get('id');
        console.log(this.pId);
      }
      );

    this.activite = this.actService.getActivit(this.pId);

    this.initForm();
  }

  initForm() {
    this.formGroup = new FormGroup({
      libelle: new FormControl(this.activite.libelle, [Validators.required]),
      description: new FormControl(this.activite.description, [Validators.required]),
    })
  }


  UpdateActivite() {
    if (this.formGroup.valid) {
      console.log("update activite");
      console.log(this.formGroup.value);
      this.activite = plainToClass(activite, this.formGroup.value);
      this.activite.id = this.pId;
      this.actService.UpdateActivite(this.activite);
      this.route.navigate(['/Admin/projets/activites']);
    }
  }

}
