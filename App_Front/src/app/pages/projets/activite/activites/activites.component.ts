import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { activite } from 'src/app/models/activite';
import { ActiviteService } from 'src/app/services/activite-service/activite.service';

declare var $: any;
declare interface TableData {
  headerRow: string[];
}

@Component({
  selector: 'app-activites',
  templateUrl: './activites.component.html',
  styleUrls: ['./activites.component.css']
})
export class ActivitesComponent implements OnInit {
  public tableData1: TableData;
  public activites: activite[];
  private querySubscription: Subscription;
  constructor(public ActivService: ActiviteService, public route: Router) { }


  ngOnInit() {

    this.querySubscription = this.ActivService.getActivite().valueChanges.subscribe((result: any) => {
      this.activites = result?.data?.activites.edges;
      //this.loading = result.loading;
      //this.error = result.error;
    });
    this.tableData1 = {
      headerRow: ['ID', 'Libelle', 'Description', 'Actions'],
    };
  }

  AjouterProjet() {
    return this.route.navigate(['Admin/projets/ajout-activite']);
  }


  UpdateActivite(act: activite) {
    console.log();
    //this.collabService.UpdateCollab(user);
    return this.route.navigate(['Admin/projets/activites/update-activite'], { queryParams: { id: act.id } });
  }

}
