import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { projet } from 'src/app/models/projet';
import { ProjetService } from 'src/app/services/projet-service/projet.service';

@Component({
  selector: 'app-ajout-projet',
  templateUrl: './ajout-projet.component.html',
  styleUrls: ['./ajout-projet.component.css']
})
export class AjoutProjetComponent implements OnInit {
  formGroup!: FormGroup;
  projet: projet;
  constructor(public projService: ProjetService, public route: Router) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.formGroup = new FormGroup({
      libelle: new FormControl("", [Validators.required]),
      description: new FormControl("", [Validators.required]),
      statut: new FormControl("", [Validators.required]),
    })
  }

  AjoutProcess() {
    console.log("Ajout process");
    this.projet = this.formGroup.value;
    console.log(this.projet);
    this.projService.AjouterProjet(this.projet);
    this.route.navigate(['/Admin/projets/details']);
  }

}
