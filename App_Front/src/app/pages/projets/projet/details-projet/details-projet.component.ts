import { Component, DoCheck, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { projet } from 'src/app/models/projet';
import { ProjetService } from 'src/app/services/projet-service/projet.service';
import Swal from 'sweetalert2';

declare var $: any;
declare interface TableData {
  headerRow: string[];
}


@Component({
  selector: 'app-details-projet',
  templateUrl: './details-projet.component.html',
  styleUrls: ['./details-projet.component.css']
})
export class DetailsProjetComponent implements OnInit, OnChanges, DoCheck {
  public tableData1: TableData;
  public projets: projet[];
  constructor(public projService: ProjetService, public route: Router) {
    //this.projets = this.projService.AfficherProjets();
  }

  ngAfterContentChecked(): void {
    //Called after every check of the component's or directive's content.
    //Add 'implements AfterContentChecked' to the class.
    console.log('Content checked done ');
    //this.projets = this.projService.AfficherProjets();
  }

  ngDoCheck(): void {
    //Called every time that the input properties of a component or a directive are checked. Use it to extend change detection by performing a custom check.
    //Add 'implements DoCheck' to the class.
    console.log('Do checked done ');
    //this.projets = this.projService.AfficherProjets();
  }

  ngOnChanges(changes: SimpleChanges): void {
    // this.projets = this.projService.AfficherProjets();
    console.log('changes done ');
  }

  ngOnInit() {
    console.log('init done ');
    this.projets = this.projService.AfficherProjets();
    console.log(this.projets);
    this.tableData1 = {
      headerRow: ['Nom du projet', 'Description', 'Statut', 'Actions'],
    };
  }

  ngAfterViewInit(): void {
    //this.ref.detectChanges();
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "Tout"]
      ],
      buttons: [

        'Ajouter'

      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Rechercher",
        "info": "Affichage _START_ à _END_ de _TOTAL_ Projets",
        "lengthMenu": "Afficher _MENU_ Projets",
        "paginate": {
          "first": "Premier",
          "last": "Dernier",
          "next": "Suivant",
          "previous": "Précédent",
        },
      }
    });

    const table = $('#datatables').DataTable();
    // Edit record
    table.on('click', '.delete', function (e) {
      let $tr = $(this).closest('tr');
      if ($($tr).hasClass('child')) {
        $tr = $tr.prev('.parent');
      }

      var data = table.row($tr).data();
      Swal.fire({
        title: 'Êtes-vous sûr?',
        text: 'Vous allez supprimer ' + data[1] + '!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Oui, je le supprime!',
        cancelButtonText: 'Non, je le garde',
        customClass: {
          confirmButton: "btn btn-success",
          cancelButton: "btn btn-danger",
        },
        buttonsStyling: false
      }).then((result) => {
        if (result.value) {
          this.projService.SupprimerCollab(data[0]);
          Swal.fire({
            title: 'Supprimé!',
            text: 'Le projet ' + data[1] + ' a été bien supprimé',
            icon: 'success',
            customClass: {
              confirmButton: "btn btn-success",
            },
            buttonsStyling: false
          });
        } else {
          Swal.fire({
            title: 'Annulé',
            text: 'Le projet ' + data[1] + ' n a pas été supprimé',
            icon: 'error',
            customClass: {
              confirmButton: "btn btn-info",
            },
            buttonsStyling: false
          });
        }
      })


      e.preventDefault();
    });


  }

  AjouterProjet() {
    return this.route.navigate(['Admin/projets/ajout-projet']);
  }

  UpdateProjet(projetd: projet) {
    console.log();
    //this.collabService.UpdateCollab(user);
    return this.route.navigate(['Admin/projets/details/update-projet'], { queryParams: { id: projetd.id } });
  }

}
