import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { projet } from 'src/app/models/projet';
import { ProjetService } from 'src/app/services/projet-service/projet.service';

@Component({
  selector: 'app-update-projet',
  templateUrl: './update-projet.component.html',
  styleUrls: ['./update-projet.component.css']
})
export class UpdateProjetComponent implements OnInit {

  formGroup!: FormGroup;
  projet: projet;
  pId: any;
  constructor(public projService: ProjetService, public route: Router, private routeA: ActivatedRoute) { }

  ngOnInit(): void {
    //recuperer l'id de l'utilisateur selectionne 
    this.routeA.queryParamMap
      .subscribe((params) => {
        console.log(params);
        this.pId = params.get('id');
        console.log(this.pId);
      }
      );

    //recuperer lr projet par son id 
    this.projet = this.projService.getProjet(this.pId);

    this.initForm();
  }

  initForm() {
    this.formGroup = new FormGroup({
      libelle: new FormControl(this.projet.libelle, [Validators.required]),
      description: new FormControl(this.projet.description, [Validators.required]),
      statut: new FormControl(this.projet.statut, [Validators.required]),
    })
    console.log(this.formGroup.value);
  }

  UpdateProcess() {
    console.log("update process");
    this.projet = this.formGroup.value;
    this.projet.id = this.pId;
    console.log(this.projet);
    this.projService.UpdateProjet(this.projet);
    this.route.navigate(['/Admin/projets/details']);
  }

}
