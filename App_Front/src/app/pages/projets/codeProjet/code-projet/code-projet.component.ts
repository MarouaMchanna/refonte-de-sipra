import { DatePipe } from '@angular/common';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { codeProjet } from 'src/app/models/codeProjet';
import { CodeProjetServiceService } from 'src/app/services/code_projet-service/code-projet-service.service';
import Swal from 'sweetalert2';

declare var $: any;
declare interface TableData {
  headerRow: string[];
}

@Component({
  selector: 'app-code-projet',
  templateUrl: './code-projet.component.html',
  styleUrls: ['./code-projet.component.css']
})
export class CodeProjetComponent implements OnInit, AfterViewInit {
  public tableData1: TableData;
  public codes: codeProjet[];
  public pipe = new DatePipe('en-US');
  constructor(public codeService: CodeProjetServiceService, public route: Router) { 
    this.codes = this.codeService.getCodeProjet();
  }

  ngOnInit() {
    this.codes = this.codeService.getCodeProjet();
    console.log(this.codes);
    this.tableData1 = {
      headerRow: ['Code', 'Description', 'Statut', 'Budget', 'Ch J/H', 'Date', 'Jira', 'Actions'],
    };
  }

  ngAfterViewInit(): void {
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "Tout"]
      ],
      buttons: [

        'Ajouter'

      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Rechercher",
        "info": "Affichage _START_ à _END_ de _TOTAL_ Codes projets",
        "lengthMenu": "Afficher _MENU_ Codes projets",
        "paginate": {
          "first": "Premier",
          "last": "Dernier",
          "next": "Suivant",
          "previous": "Précédent",
        },
      }
    });

    const table = $('#datatables').DataTable();

    // delete record
    table.on('click', '.delete', function (e) {
      let $tr = $(this).closest('tr');
      if ($($tr).hasClass('child')) {
        $tr = $tr.prev('.parent');
      }

      var data = table.row($tr).data();
      Swal.fire({
        title: 'Êtes-vous sûr?',
        text: 'Vous allez supprimer ' + data[1] + '!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Oui, je le supprime!',
        cancelButtonText: 'Non, je le garde',
        customClass: {
          confirmButton: "btn btn-success",
          cancelButton: "btn btn-danger",
        },
        buttonsStyling: false
      }).then((result) => {
        if (result.value) {
          this.codeService.SupprimerCProjet(data[0]);
          Swal.fire({
            title: 'Supprimé!',
            text: 'Le projet ' + data[0] + ' a été bien supprimé',
            icon: 'success',
            customClass: {
              confirmButton: "btn btn-success",
            },
            buttonsStyling: false
          });
        } else {
          Swal.fire({
            title: 'Annulé',
            text: 'Le code projet ' + data[0] + ' n a pas été supprimé',
            icon: 'error',
            customClass: {
              confirmButton: "btn btn-info",
            },
            buttonsStyling: false
          });
        }
      })
      e.preventDefault();
    });


  }

  getDiffDays(date2) {

    var ToDay = this.pipe.transform(Date.now(), 'yyyy/MM/dd h:mm:ss a zzzz');
    var tmp = Date.parse(date2) - Date.parse(ToDay);
    var day = tmp / (1000 * 3600 * 24);
    if (day > 0)
      return Math.floor(day);
    else
      return 0;
  }

  getFirstPeriode(date1, date2) {
    var tmp = Date.parse(date2) - Date.parse(date1);
    var day = tmp / (1000 * 3600 * 24);
    if (day > 0)
      return Math.floor(day);
    else
      return 0;
  }

  getPecComplete(c: any) {
    var d = (this.getDiffDays(c.dateFin) * 100) / this.getFirstPeriode(c.dateDebut, c.dateFin);
    d = 100 - d;
    if (d)
      return d.toFixed(2);
    else
      return 0;

  }

  getBudgetConsom(c: any) {
    var var1 = parseInt(c.budgetNRJconsomme);
    var var2 = parseInt(c.budgetDECOconsomme);
    var var3 = parseInt(c.budgetCLOEconsomme);
    var bc = var1 + var2 + var3;
    if (bc)
      return bc;
    else
      return 0;
  }

  getChargeConsom(c: any): any {
    var var1 = parseInt(c.chargeNRJconsomme);
    console.log(c.chargeCLOEconsomme);
    var var2 = parseInt(c.chargeDECOconsomme);
    var var3 = parseInt(c.chargeCLOEconsomme);
    var cc = var1 + var2 + var3;
    console.log(cc);
    if (cc)
      return cc;
    else
      return 0;
  }

  AjouterCodeProjet() {
    return this.route.navigate(['Admin/projets/ajout-codeprojet']);
  }

  UpdateCodeProjet(Cprojet: codeProjet) {
    return this.route.navigate(['Admin/projets/codeProjet/update-code-projet'], { queryParams: { id: Cprojet.id } });
  }

}
