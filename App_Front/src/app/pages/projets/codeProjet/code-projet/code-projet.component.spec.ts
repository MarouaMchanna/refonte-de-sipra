import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeProjetComponent } from './code-projet.component';

describe('CodeProjetComponent', () => {
  let component: CodeProjetComponent;
  let fixture: ComponentFixture<CodeProjetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CodeProjetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeProjetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
