import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { plainToClass } from 'class-transformer';
import { codeProjet } from 'src/app/models/codeProjet';
import { CodeProjetServiceService } from 'src/app/services/code_projet-service/code-projet-service.service';

@Component({
  selector: 'app-update-code-projet',
  templateUrl: './update-code-projet.component.html',
  styleUrls: ['./update-code-projet.component.css'],
  providers: [DatePipe],
})
export class UpdateCodeProjetComponent implements OnInit {
  formGroup!: FormGroup;
  pId: any;
  cp: codeProjet;
  constructor(public route: Router, private routeA: ActivatedRoute, public CPservice: CodeProjetServiceService, private datePipe: DatePipe) { }

  ngOnInit(): void {
    //recuperer l'id du code projet selectionne 
    this.routeA.queryParamMap
      .subscribe((params) => {
        //console.log(params);
        this.pId = params.get('id');
        console.log(this.pId);
      }
      );
    this.cp = plainToClass(codeProjet, this.CPservice.getCProjet(this.pId));
    //this.cp.budget_cloe = this.cp.
    console.log(this.cp);

    //initialiser le formulaire avec l'objet recuperer 
    this.initForm();

  }

  initForm() {
    this.formGroup = new FormGroup({
      libelle: new FormControl(this.cp.libelle, [Validators.required]),
      description: new FormControl(this.cp.description, [Validators.required]),
      budgetNRJ: new FormControl(this.cp.budgetNRJ, [Validators.required]),
      budgetDECO: new FormControl(this.cp.budgetDECO, [Validators.required]),
      budgetCLOE: new FormControl(this.cp.budgetCLOE, [Validators.required]),
      budget: new FormControl(this.cp.budget, [Validators.required]),
      charge: new FormControl(this.cp.charge, [Validators.required]),
      chargeNRJ: new FormControl(this.cp.chargeNRJ, [Validators.required]),
      chargeDECO: new FormControl(this.cp.chargeDECO, [Validators.required]),
      chargeCLOE: new FormControl(this.cp.chargeCLOE, [Validators.required]),
      dateDebut: new FormControl(this.cp.dateDebut, [Validators.required]),
      dateFin: new FormControl(this.cp.dateFin, [Validators.required]),
      statut: new FormControl(this.cp.statut, [Validators.required]),

    })

  }

  UpdateProcess() {

    if (this.formGroup.valid) {
      this.cp = plainToClass(codeProjet, this.formGroup.value);

      this.cp.id = this.pId;

      var datedebut = this.datePipe.transform(this.formGroup.get('dateDebut').value
        , 'yyyy-MM-dd');
      var dateFin = this.datePipe.transform(this.formGroup.get('dateFin').value
        , 'yyyy-MM-dd');

      this.cp.dateFin = dateFin;
      this.cp.dateDebut = datedebut;

      this.CPservice.UpdateCodeProjet(this.cp);
      this.route.navigate(['/Admin/projets/codeProjet']);
      this.formGroup.reset();
    }
  }
}
