import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCodeProjetComponent } from './update-code-projet.component';

describe('UpdateCodeProjetComponent', () => {
  let component: UpdateCodeProjetComponent;
  let fixture: ComponentFixture<UpdateCodeProjetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateCodeProjetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCodeProjetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
