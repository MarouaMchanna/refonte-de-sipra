import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutCodeProjetComponent } from './ajout-code-projet.component';

describe('AjoutCodeProjetComponent', () => {
  let component: AjoutCodeProjetComponent;
  let fixture: ComponentFixture<AjoutCodeProjetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjoutCodeProjetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutCodeProjetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
