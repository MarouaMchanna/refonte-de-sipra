import { DatePipe, formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { plainToClass } from 'class-transformer';
import { codeProjet } from 'src/app/models/codeProjet';
import { CodeProjetServiceService } from 'src/app/services/code_projet-service/code-projet-service.service';


formatDate(new Date(), 'yyyy/MM/dd', 'en');
@Component({
  selector: 'app-ajout-code-projet',
  templateUrl: './ajout-code-projet.component.html',
  styleUrls: ['./ajout-code-projet.component.css'],
  providers: [DatePipe],
})
export class AjoutCodeProjetComponent implements OnInit {
  public formGroup!: FormGroup;
  public codeP: codeProjet;
  constructor(public CPservice: CodeProjetServiceService, private datePipe: DatePipe, public route: Router) { }


  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.formGroup = new FormGroup({
      libelle: new FormControl("", [Validators.required]),
      description: new FormControl("", [Validators.required]),
      budgetNRJ: new FormControl("", [Validators.required]),
      budgetDECO: new FormControl("", [Validators.required]),
      budgetCLOE: new FormControl("", [Validators.required]),
      budget: new FormControl("", [Validators.required]),
      charge: new FormControl("", [Validators.required]),
      chargeNRJ: new FormControl("", [Validators.required]),
      chargeDECO: new FormControl("", [Validators.required]),
      chargeCLOE: new FormControl("", [Validators.required]),
      dateDebut: new FormControl(new Date(), [Validators.required]),
      dateFin: new FormControl(new Date(), [Validators.required]),
      statut: new FormControl(false, [Validators.required]),

    })

  }

  get libelle(): FormControl {
    return this.formGroup.get("libelle") as FormControl;
  }
  get description(): FormControl {
    return this.formGroup.get("description") as FormControl;
  }
  get budgetNRJ(): FormControl {
    return this.formGroup.get("budgetNRJ") as FormControl;
  }
  get budgetDECO(): FormControl {
    return this.formGroup.get("budgetDECO") as FormControl;
  }
  get budgetCLOE(): FormControl {
    return this.formGroup.get("budgetCLOE") as FormControl;
  }
  get chargeNRJ(): FormControl {
    return this.formGroup.get("chargeNRJ") as FormControl;
  }
  get chargeDECO(): FormControl {
    return this.formGroup.get("chargeDECO") as FormControl;
  }
  get chargeCLOE(): FormControl {
    return this.formGroup.get("budgetCLOE") as FormControl;
  }
  get budget(): FormControl {
    return this.formGroup.get("budget") as FormControl;
  }
  get charge(): FormControl {
    return this.formGroup.get("charge") as FormControl;
  }
  get dateDebut(): FormControl {
    return this.formGroup.get("dateDebut") as FormControl;
  }
  get dateFin(): FormControl {
    return this.formGroup.get("dateFin") as FormControl;
  }
  get statut(): FormControl {
    return this.formGroup.get("statut") as FormControl;
  }


  //Ajouter user
  AjoutProcess() {
    if (this.formGroup.valid) {

      this.codeP = plainToClass(codeProjet, this.formGroup.value);

      var datedebut = this.datePipe.transform(this.formGroup.get('dateDebut').value
        , 'yyyy-MM-dd');
      var dateFin = this.datePipe.transform(this.formGroup.get('dateFin').value
        , 'yyyy-MM-dd');

      this.codeP.dateFin = dateFin;
      this.codeP.dateDebut = datedebut;

      console.log(this.codeP);

      this.CPservice.AjouterCodeProjet(this.codeP);
      this.route.navigate(['/Admin/projets/codeProjet']);
    }
    this.formGroup.reset();
  }

}
