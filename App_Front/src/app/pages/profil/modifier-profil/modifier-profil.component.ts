import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { plainToClass } from 'class-transformer';
import { user } from 'src/app/models/user';
import { CollaborateurService } from 'src/app/services/collaborateur-service/collaborateur.service';
import bcrypt from 'bcryptjs';

@Component({
  selector: 'app-modifier-profil',
  templateUrl: './modifier-profil.component.html',
  styleUrls: ['./modifier-profil.component.css']
})
export class ModifierProfilComponent implements OnInit {

  formGroupInfoPerso:FormGroup;
  formGroupPassword:FormGroup;
  collaborateur:any;
  urlImage:String;
  siteD:any;
  emailD:any;
  imageD:any;
  photoUrl:String;
  Cpass: String = 'false';
  hash:String='false';
  user:user;
  mdp:string;
  collaborateur2:any;

  constructor(private collabService : CollaborateurService) { }

  ngOnInit(): void {
 
  this.collaborateur=this.collabService.getCollabByEmail(localStorage.getItem("username"));
  this.collaborateur.forEach(c => {
  this.urlImage="http://localhost:8000/uploads/images/users/"+c.node.image;
  this.imageD=c.node.image;
  this.siteD=c.node.site;
  this.emailD=c.node.email;
  });
  this.formInit();
  console.log(this.siteD);
  console.log(this.urlImage);
  }

  //initialisation des deux formulaires
  formInit(){
    this.formGroupInfoPerso = new FormGroup({
      email: new FormControl(this.emailD, [Validators.required,Validators.email]),
      site: new FormControl(this.siteD, [Validators.required]),
      image:new FormControl("", []),
      fileSource: new FormControl('', [Validators.required])
      
    });
    this.formGroupPassword=new FormGroup({
      AncienPassword:new FormControl("",[Validators.required]),
      password:new FormControl("",[Validators.required]),
      Vpassword:new FormControl("",[Validators.required]),
    });

  }

  // recuperer les formControl afin de faire la validation du formulaire
  get AncienPassword(): FormControl {
    return this.formGroupPassword.get("AncienPassword") as FormControl;
  }
  get password(): FormControl {
    return this.formGroupPassword.get("password") as FormControl;
  }
  get Vpassword(): FormControl {
    return this.formGroupPassword.get("Vpassword") as FormControl;
  }
  
  // quand on choisi une image on fait appel a cette fct 
  onFileChange(event) {
    const reader = new FileReader();
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.photoUrl = reader.result as string;
        console.log(this.photoUrl);
        this.formGroupInfoPerso.patchValue({
          fileSource: reader.result

        });
      };
    }
  }

  // methode pour modifier les info personnelles apres l'envoi du 1 er form
  ModifierInfoPerso(){
    if (this.formGroupInfoPerso.valid) {
     // console.log(this.formGroupInfoPerso.value);
     this.collaborateur=plainToClass(user,this.formGroupInfoPerso.value);
    }
       
    var data = {
      "fileSource" : this.photoUrl, 
      "nom_image" : localStorage.getItem("nom")+localStorage.getItem("CODE")
  };
  console.log(data);
  this.collaborateur.image=localStorage.getItem("nom")+localStorage.getItem("CODE")+".png";
  console.log(this.collaborateur.image);
    this.collabService.AjouterImage(data).subscribe(result=>{
      if(result.error){
        console.log("function Ajouter image doesn't work");
      }
      else{
        console.log("from ajouer image backend "+result.fileSource);
        localStorage.setItem("image",this.collaborateur.image);
      }
    });
    this.collaborateur.id=localStorage.getItem("id");
    console.log(this.collaborateur.id);
    this.collabService.modifierInfoP(this.collaborateur);
    
    
  }
  // methode pour modifier le md apres l'envoi du 2 eme form
  ModifierPassword(){

    if(this.formGroupPassword.valid){
      //récuperation du mdp à partir de la BDD
      this.collaborateur2=this.collabService.getCollabByEmail(localStorage.getItem("username"));
      this.collaborateur2.forEach(c2 => {
      this.mdp=c2.node.password; 
      
      });
      this.Cpass = 'false';
      this.hash='false';
      //validation des champs mot de passe et sa confirmation
      if(this.password.value!=this.Vpassword.value){
        this.Cpass = 'true';
      }
      else{
      //on compare le champ saisi par l'utilisateur "ancien mot de passe" avec le mot de passe récuperé de la BDD
      bcrypt.compare(this.AncienPassword.value, this.mdp, (err, res) => {
        //Si on a erreur dans cette methode
        if (err){
          console.log("erreur dans la comparaison des mots de passe")
        }
        if (res){
          // cas de success 
          //on chiffre le nouveau mot de passe saisi par l'utilisateur et on l'ajoute à notre BDD 
          console.log("success");
          var hashedNewPassword = bcrypt.hashSync(this.password.value, 8);
          this.collabService.modifiermdp(localStorage.getItem('id'),hashedNewPassword);
          console.log(hashedNewPassword);
          console.log("yeeey");
          this.formGroupPassword.reset();
        } else {
          this.hash='true';
          console.log('passwords do not match');
        }
      
      });
   }
  }
}
}
