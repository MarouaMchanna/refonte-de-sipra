import { Component, OnInit } from '@angular/core';
import { user } from 'src/app/models/user';
import { CollaborateurService } from 'src/app/services/collaborateur-service/collaborateur.service';

@Component({
  selector: 'app-details-profil',
  templateUrl: './details-profil.component.html',
  styleUrls: ['./details-profil.component.css']
})
export class DetailsProfilComponent implements OnInit {
  collaborateur:any;
  urlImage:String;

  constructor(private collabService : CollaborateurService) { }

  ngOnInit(): void {
  this.collaborateur=this.collabService.getCollabByEmail(localStorage.getItem("username"));
  this.collaborateur.forEach(c => {
    this.urlImage="http://localhost:8000/uploads/images/users/"+c.node.image;
  });
  console.log(this.urlImage);
  
  }

}
