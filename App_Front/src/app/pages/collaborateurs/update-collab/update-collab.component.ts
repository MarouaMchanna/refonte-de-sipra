import { Component, OnInit } from '@angular/core';
import { FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { CollaborateurService } from 'src/app/services/collaborateur-service/collaborateur.service';
import { ProjetService } from 'src/app/services/projet-service/projet.service';
import { ActivatedRoute, Router } from '@angular/router';
import { user } from 'src/app/models/user';
import { projet } from 'src/app/models/projet';
import bcrypt from 'bcryptjs';

@Component({
  selector: 'app-update-collab',
  templateUrl: './update-collab.component.html',
  styleUrls: ['./update-collab.component.css']
})
export class UpdateCollabComponent implements OnInit {

  formGroup!: FormGroup;
  userId: any;
  user: user;
  public projets: projet[];
  modeselect: any;
  public isCollab: boolean;
  user2: user;

  constructor(private router: Router, private collabService: CollaborateurService, private route: ActivatedRoute, private projetServ: ProjetService) { }


  Postes = [
    { value: 'Développeur', viewValue: 'Développeur' },
    { value: 'Testeur', viewValue: 'Testeur' },
    { value: 'Analyste-développeur', viewValue: 'Analyste-développeur' },];

  // Postes: [{ viewValue: string }];



  ngOnInit(): void {

    //recuperer l'id de l'utilisateur selectionne 
    this.route.queryParamMap
      .subscribe((params) => {
        console.log(params);
        this.userId = params.get('id');
        console.log(this.userId);
      }
      );


    this.user = this.collabService.getCollab(this.userId);
    console.log(this.user.image);
    this.initForm();





    //initialiser le formulaire
    this.initForm();
    //afficher les libelles des projets a l'aide du service affiherProjets
    this.projets = this.projetServ.AfficherProjets();

    //verifier si l'utilisateur est un admin pour checker le radio button
    this.isAdmin();

    //afficher les libelles des poste depuis les utilisateurs  
    this.formGroup.setValue({ projet: this.user.projet.libelle });


  }


  // initialiser le formulaire a l'aide des attributs de l'utilisateur recupere 
  initForm() {
    this.formGroup = new FormGroup({
      Nom: new FormControl(this.user.nom, [Validators.required]),
      email: new FormControl(this.user.email, [Validators.required]),
      codeCAPIT: new FormControl(this.user.codecapit, [Validators.required]),
      Site: new FormControl(this.user.site, [Validators.required]),
      CJM: new FormControl(this.user.CJM, [Validators.required]),
      projet: new FormControl("", [Validators.required]),
      poste: new FormControl(this.user.poste, [Validators.required]),
      password: new FormControl("", [Validators.required]),
      Vpassword: new FormControl("", [Validators.required]),
      roles: new FormControl("", [Validators.required]),
      img: new FormControl("", [Validators.required]),

    })

  }


  //la fonction qui recupere les nouvelles valeurs et les transmets au service update 
  UpdateProcess() {


    console.log("update");
    // if (this.formGroup.valid) {
    //console.log(this.formGroup.value);
    this.user2 = this.formGroup.value;
    this.user2.id = this.userId;

    //decoder le nouveau password avec l'algo bcrypt 
    var hashedPassword = bcrypt.hashSync(this.user2.password, 8);
    this.user2.password = hashedPassword;
    console.log(this.user2.password);

    bcrypt.compare(this.formGroup.get('Vpassword').value, this.user2.password, (err, res) => {

      if (err) {
        console.log("Un erreur c'est produit, Veuillez ressayer !!");
      }
      if (res) {
        this.user2.image = this.formGroup.value.img;
        this.user2.site = this.formGroup.value.Site;
        this.user2.codecapit = this.formGroup.value.codeCAPIT;
        if (this.formGroup.value.roles.includes("1")) {
          this.user2.roles = ["ROLE_USER"];
        } else { this.user2.roles = ["ROLE_ADMIN"]; }
        console.log(this.formGroup.value.img);

        //faire appel au service update en envoyant le nouveau utilisateur 
        this.collabService.UpdateCollab(this.user2);
        console.log("update done");
        this.router.navigate(['/Admin/Collaborateurs']);
      }
      else {
        console.log("Le mot de passe et la validation ne sont pas compatible, Veuillez ressayer !!");
      }
    });
    //}
  }

  //verifier si l'utilisateur est un admin pour checker le radio button
  isAdmin() {
    if (this.user.roles.indexOf("ROLE_ADMIN") > -1) {
      this.isCollab = false;
    } else { this.isCollab = true; }
    return this.isCollab;
  }
}


