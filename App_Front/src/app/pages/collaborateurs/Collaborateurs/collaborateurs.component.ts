import { Component, OnInit, AfterViewInit,ChangeDetectorRef  } from '@angular/core';
import { Router } from '@angular/router';
import { user } from 'src/app/models/user';
import { CollaborateurService } from '../../../services/collaborateur-service/collaborateur.service';
import Swal from 'sweetalert2';
//src\app\services\collaborateur-service
declare var $: any;
declare interface TableData {
  headerRow: string[];
}

@Component({
  selector: 'app-collaborateurs',
  templateUrl: './collaborateurs.component.html',
  styleUrls: ['./collaborateurs.component.css']
})
export class CollaborateursComponent implements OnInit, AfterViewInit {
  public tableData1: TableData;
  collab: user[];
  constructor(private route: Router,private collabService : CollaborateurService,private ref:ChangeDetectorRef ,private rout: Router) { 
    console.log("constructeur");
  }

  ngOnInit() {
    console.log("ngoninit");
    this.collabService.getColla().subscribe((result: any) => {
      this.collab = result?.data?.users.edges;
    });
    this.tableData1 = {
      headerRow: ['Code Capit', 'Nom', 'Adresse éléctronique', 'Projet', 'Poste', 'Role', 'Actions'],
    };
  }


  ngAfterViewInit(): void {
    
  
    console.log("ngAfterViewInit");
    this.ref.detectChanges();
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "Tout"]
      ],
      buttons: [

        'Ajouter'

      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Rechercher",
        "info": "Affichage _START_ à _END_ de _TOTAL_ collaborateurs",
        "lengthMenu": "Afficher _MENU_ Collaborateurs",
        "paginate": {
          "first": "Premier",
          "last": "Dernier",
          "next": "Suivant",
          "previous": "Précédent",
        },
      }
    });


    const table = $('#datatables').DataTable();
    const serviceCollab = this.collabService;
    // Edit record
    table.on('click', '.delete', function (e) {
      let $tr = $(this).closest('tr');
      if ($($tr).hasClass('child')) {
        $tr = $tr.prev('.parent');
      }

      var data = table.row($tr).data();
      Swal.fire({
        title: 'Êtes-vous sûr?',
        text: 'Vous allez supprimer ' + data[1] + '!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Oui, je le supprime!',
        cancelButtonText: 'Non, je le garde',
        customClass: {
          confirmButton: "btn btn-success",
          cancelButton: "btn btn-danger",
        },
        buttonsStyling: false
      }).then((result) => {
        if (result.value) {  
          serviceCollab.SupprimerCollab(data[7]);
          Swal.fire({
            title: 'Supprimé!',
            text: 'Le collaborateur ' + data[1] + ' a été bien supprimé',
            icon: 'success',
            customClass: {
              confirmButton: "btn btn-success",
            },
            buttonsStyling: false
          });
        } else {
          Swal.fire({
            title: 'Annulé',
            text: 'Le collaborateur ' + data[1] + ' n a pas été supprimé',
            icon: 'error',
            customClass: {
              confirmButton: "btn btn-info",
            },
            buttonsStyling: false
          });
        }
      })


      e.preventDefault();
    });

    this.rout.navigate(['/Admin/Collaborateurs']);
  }

  AjouterCollab(){
    //this.collabService.getCollabByEmail();
   // this.collabService.SupprimerCollab("/api/users/15");
    //this.collabService.AjouterCollab();
    //this.collabService.AjoutActivite("libelle2","description1");
    console.log("ajout");
    return this.route.navigate(['Admin/collaborateurs/Ajout']);
  }

  UpdateCollab(userd: user) {
   // console.log();
    //this.collabService.UpdateCollab(user);
    return this.route.navigate(['Admin/Collaborateurs/Update'], { queryParams: { id: userd.id } });
  }
}


