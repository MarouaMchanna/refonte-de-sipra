import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { plainToClass } from 'class-transformer';
import { user } from 'src/app/models/user';
import { CollaborateurService } from '../../../services/collaborateur-service/collaborateur.service';
import { ProjetService } from '../../../services/projet-service/projet.service';
import { projet } from 'src/app/models/projet';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-ajout-collab',
  templateUrl: './ajout-collab.component.html',
  styleUrls: ['./ajout-collab.component.css']
})
export class AjoutCollabComponent implements OnInit {
  formGroup!: FormGroup;
  collaborateur: user;
  projets: projet[];
  Cpass: String = 'false';
  photoUrl: String;
  photo: any;
  constructor(private collabService: CollaborateurService, private projetService: ProjetService, private route: Router) { }
  Postes = [
    { value: 'Développeur', viewValue: 'Développeur' },
    { value: 'Testeur', viewValue: 'Testeur' },
    { value: 'Analyste-développeur', viewValue: 'Analyste-développeur' },

  ];

  ngOnInit(): void {
    this.initForm();
    this.projets = this.projetService.AfficherProjets();
    this.image.valueChanges.subscribe((v) => {
      this.photoUrl = v;
    });
    console.log(this.photoUrl);
  }
  initForm() {
    this.formGroup = new FormGroup({
      nom: new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.required, Validators.email]),
      codecapit: new FormControl("", [Validators.required]),
      site: new FormControl("", [Validators.required]),
      cjm: new FormControl("", [Validators.required]),
      projet_id: new FormControl("", [Validators.required]),
      poste: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required]),
      Vpassword: new FormControl("", [Validators.required]),
      roles: new FormControl(['1'], [Validators.required]),
      image: new FormControl("", []),
      fileSource: new FormControl('', [Validators.required])

    })
  }

  get Nom(): FormControl {
    return this.formGroup.get("nom") as FormControl;
  }
  get Email(): FormControl {
    return this.formGroup.get("email") as FormControl;
  }
  get codecapit(): FormControl {
    return this.formGroup.get("codecapit") as FormControl;
  }
  get site(): FormControl {
    return this.formGroup.get("site") as FormControl;
  }
  get cjm(): FormControl {
    return this.formGroup.get("cjm") as FormControl;
  }
  get projet_id(): FormControl {
    return this.formGroup.get("projet_id") as FormControl;
  }
  get poste(): FormControl {
    return this.formGroup.get("poste") as FormControl;
  }
  get password(): FormControl {
    return this.formGroup.get("password") as FormControl;
  }
  get Vpassword(): FormControl {
    return this.formGroup.get("Vpassword") as FormControl;
  }
  get image(): FormControl {
    return this.formGroup.get("image") as FormControl;
  }

  onFileChange(event) {

    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.photoUrl = reader.result as string;
        console.log(this.photoUrl);
        this.formGroup.patchValue({
          fileSource: reader.result

        });
      };
    }
  }

  //Ajouter user
  AjoutProcess() {
    if (this.formGroup.valid) {
      console.log(this.formGroup.value);
      if (this.password.value == this.Vpassword.value) {
        this.collaborateur = plainToClass(user, this.formGroup.value);
        this.collaborateur.CJM = this.formGroup.get('cjm').value;
        console.log(this.collaborateur);
        if (this.collaborateur.roles.includes("1")) {
          this.collaborateur.roles = ["ROLE_USER"];

        } else {
          this.collaborateur.roles = ["ROLE_ADMIN"];
        }
        //console.log(this.collaborateur);

        var data = {
          "fileSource": this.photoUrl,
          "nom_image": this.collaborateur.nom + this.collaborateur.codecapit
        };
        //console.log(data);
        this.collaborateur.CJM=this.formGroup.get("cjm").value;
        this.collaborateur.image = this.collaborateur.nom + this.collaborateur.codecapit + ".png";
        console.log(this.collaborateur);
        this.collabService.AjouterCollab(this.collaborateur);
        this.collabService.AjouterImage(data).subscribe(result => {
          if (result.error) {
            console.log("function Ajouter image doesn't work");
          }
          else {
            console.log("from ajouer image backend " + result.fileSource);
          }
        });
        
        
        Swal.fire({
          title: "L'opération a bien réussi",
          text: "Le collaborateur a été ajouté avec succés!",
          buttonsStyling: false,
          customClass: {
            confirmButton: "btn btn-success",
          },
          icon: "success"
        });

        this.route.navigate(['/Admin/Collaborateurs']);

      }
      else {
        this.Cpass = 'true';
      }
    }
  }
}
