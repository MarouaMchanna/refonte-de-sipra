
import { AfterViewInit, Component, OnInit, ElementRef, Injectable } from '@angular/core';
import { CodeProjetServiceService } from '../../services/code_projet-service/code-projet-service.service';
import { codeProjet } from 'src/app/models/codeProjet';
import { ActiviteService } from '../../services/activite-service/activite.service';
import { activite } from 'src/app/models/activite';
import { TacheService } from 'src/app/services/tache-service/tache.service';
import { tache } from 'src/app/models/tache';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import {
  MatDateRangeSelectionStrategy,
  DateRange,
  MAT_DATE_RANGE_SELECTION_STRATEGY,
} from '@angular/material/datepicker';

import * as FileSaver from 'file-saver';
import { DetailsImputationService } from 'src/app/services/details-imputation-service/details-imputation.service';
import { detailsImputation } from 'src/app/models/detailsImputation';
import { ImputationService } from 'src/app/services/imputation-service/imputation.service';
import { CollaborateurService } from 'src/app/services/collaborateur-service/collaborateur.service';
import { user } from 'src/app/models/user';
import { DatePipe } from '@angular/common';
import { plainToClass } from 'class-transformer';
import { imputationDetails } from 'src/app/models/imputationDetails';
import { Subscription } from 'rxjs';
import { imputation } from 'src/app/models/imputation';


@Injectable()
export class FiveDayRangeSelectionStrategy<D> implements MatDateRangeSelectionStrategy<D> {
  constructor(private _dateAdapter: DateAdapter<D>) { }

  selectionFinished(date: D | null): DateRange<D> {
    return this._createFiveDayRange(date);
  }

  createPreview(activeDate: D | null): DateRange<D> {
    return this._createFiveDayRange(activeDate);
  }

  private _createFiveDayRange(date: D | null): DateRange<D> {
    if (date) {
      const start = this._dateAdapter.addCalendarDays(date, 0);
      const end = this._dateAdapter.addCalendarDays(date, 4);
      return new DateRange<D>(start, end);
    }

    return new DateRange<D>(null, null);
  }
}



declare var $: any;
declare const require: any;

declare interface TableData {
  headerRow: String[];
  footerRow: String[];
  dataRows: string[][];
}

@Component({
  selector: 'app-imputation-cmp',
  templateUrl: './imputation.component.html',
  styles: ['md-calendar{width:300px; }'],
  providers: [
    {
      provide: MAT_DATE_RANGE_SELECTION_STRATEGY,
      useClass: FiveDayRangeSelectionStrategy,
    },
  ],
})
export class ImputationComponent implements OnInit, AfterViewInit {
  public tableData1: TableData;
  codeP: codeProjet[];
  activites: any[] = [];
  taches: tache[];
  collab: user[];
  detailsImputationValue: String[] = [];
  imputations: any[];
  details: any[];
  detailsImputation: detailsImputation;
  detailsImputaFinal: any[];
  Imputation: imputation;
  exportList: any[] = [];
  public code: String;
  public tache: String;
  public activite: String[];
  public admin: boolean = false;
  public AddClick: Boolean = false;
  public recherche: Boolean = false;
  public expoSemaine: Boolean = false;
  public expoMois: Boolean = false;
  dateList: String[] = [];
  nom: any;
  id: any;
  i: String;

  private querySubscription: Subscription;


  constructor(private cpService: CodeProjetServiceService, private DetailsImpService: DetailsImputationService,
    private actService: ActiviteService, private tchService: TacheService, private imputationService: ImputationService,
    private collabService: CollaborateurService) {
  }


  public ImputSearhForm = new FormGroup({
    collab: new FormControl(),
    start: new FormControl(),
    end: new FormControl(),
  });

  ngOnInit() {
    this.tableData1 = {
      headerRow: ['#', 'Code Projet', 'Taches', 'Activitees', 'Lun ', 'Mar ', 'Mer ', 'Jeu ', 'Vend ', 'Total', 'Commentaire'],
      footerRow: [],
      dataRows: []
    };

    if (localStorage.getItem("roles").indexOf("ROLE_ADMIN") > -1) {
      this.admin = true;
    }
    this.nom = localStorage.getItem("nom");
    this.id = localStorage.getItem("id");
    console.log("hello user");
    console.log(this.id + "   " + this.nom);
    // this.collab[0] = this.collabService.getCollab(this.id);
    this.collab = this.collabService.getCollabs();
    this.codeP = this.cpService.getCodeProjet();

    this.taches = this.tchService.getTache();
    //this.AfficherImputation();


  }


  ngAfterViewInit() {
    $('#table').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
    });

    const table = $('#table').DataTable();

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });
    // save a record
    table.on('click', '.save', function (e) {
      e.preventDefault();
    });

  }

  AddImp() {
    console.log(this.AddClick);
    if (this.AddClick == false) {
      this.AddClick = true;
      //console.log(this.AddClick);
      this.querySubscription = this.actService.getActivite().valueChanges.subscribe((result: any) => {
        this.activites = result?.data?.activites.edges;
        console.log(this.activites);
        //this.loading = result.loading;
        //this.error = result.error;
      }, (error: any) => {
        console.log("le service get activite ne marche pas ");
      });
    }
    else {
      this.AddClick = false;
      //console.log(this.AddClick);
    }
  }

  test(formI: NgForm) {
    this.detailsImputationValue = [];
    console.log(this.detailsImputationValue);
    this.Imputation = new imputation();
    this.Imputation.commentaire = formI.form.get("commentaire").value;
    this.Imputation.user_id = this.ImputSearhForm.get("collab").value;
    this.i = this.imputationService.AjouterImputation(this.Imputation);
    console.log(this.i);
    this.detailsImputation = new detailsImputation();
    // this.detailsImputation = plainToClass(detailsImputation, formI.form.value);
    this.detailsImputation.activite_id = formI.form.get("activite").value;
    this.detailsImputation.tache_id = formI.form.get("tache").value;
    this.detailsImputation.imputation_id = this.i;
    this.detailsImputation.codeprojet_id = formI.form.get("code-projet").value;
    //console.log(formI.form.get("lun").value);
    this.detailsImputationValue.push(formI.form.get("lun").value);
    this.detailsImputationValue.push(formI.form.get("mar").value);
    this.detailsImputationValue.push(formI.form.get("mer").value);
    this.detailsImputationValue.push(formI.form.get("jeu").value);
    this.detailsImputationValue.push(formI.form.get("ven").value);
    console.log(this.detailsImputationValue);
    for (let j of [0, 1, 2, 3, 4]) {
      this.detailsImputation.date = this.dateList[j];
      if (this.detailsImputationValue[j] == null) {
        this.detailsImputation.valeur = "0";
      }
      this.detailsImputation.valeur = this.detailsImputationValue[j];
      this.DetailsImpService.AjouterImputation(this.detailsImputation);
    }
    console.log(this.detailsImputation);
    formI.form.reset();
    this.recherche = true;
    this.expoSemaine = false;
    this.onSubmit();
    //this.formI.get("")
    // this.tableData1.dataRows.push(formI.value);
  }


  myFilter = (d: Date | null): boolean => {
    const day = (d || new Date()).getDay();
    // Prevent Saturday and Sunday from being selected.
    return day == 1;
  };

  getCodeProjet(detailsImp: any[]) {
    detailsImp.forEach(element => {
      this.code = element.node.codeprojet.libelle;
    });
    return this.code;
  }
  getTache(detailsImp: any[]) {
    detailsImp.forEach(element => {
      this.tache = element.node.tache.libelle;
    });
    return this.tache;
  }
  getActivite(detailsImp: any[]) {
    detailsImp.forEach(element => {
      this.activite = element.node.activite.libelle;
    });
    return this.activite;
  }

  getTotal(detailsImp: any[]) {
    let total = 0.00;
    detailsImp.forEach(element => {
      total += parseFloat(element.node.valeur);
    });

    return total;
  };


  onSubmit(): void {
    if (this.recherche) {
      this.tableData1.dataRows = [];
      console.log("recherche");
      console.log(this.ImputSearhForm.value);
      this.tableData1.dataRows = [];
      this.getWeek(this.ImputSearhForm.get("start").value);
      this.tableData1.dataRows = this.imputationService.getImputation(this.dateList, this.ImputSearhForm.get("collab").value);
      console.log(this.tableData1.footerRow);
    }
    if (this.expoSemaine) {
      console.log("expo");
      console.log(this.ImputSearhForm.value);
      this.getWeek(this.ImputSearhForm.get("start").value);
      this.details = [];
      this.details = this.DetailsImpService.getDetailsImputation(this.dateList);
      var result = arr =>
        arr.map(({ node: { id, date, valeur, imputation, codeprojet, tache, activite } }) => ({
          id,
          date,
          valeur,
          id_imputation: imputation.id,
          commentaire: imputation.commentaire,
          nom: imputation.user.nom,
          roles: imputation.user.roles,
          codecapit: imputation.user.codecapit,
          codeprojet: codeprojet.libelle,
          tache: tache.libelle,
          activite: activite.libelle,
        }));
      console.log("details");
      console.log(result(this.details));

      if (this.details) {
        this.exportList = result(this.details);
        console.log(this.exportList);
        this.exportExcel()
      }
    }
  }
  Rechercher() {
    this.expoSemaine = false;
    this.recherche = true;
    this.onSubmit();
  }
  ExporterSema() {
    this.recherche = false;
    this.expoSemaine = true;
    console.log("expo");
    console.log(this.ImputSearhForm.value);
    this.getWeek(this.ImputSearhForm.get("start").value);
    this.details = [];
    this.details = this.DetailsImpService.getDetailsImputation(this.dateList);
    var result = arr =>
      arr.map(({ node: { id, date, valeur, imputation, codeprojet, tache, activite } }) => ({
        id,
        date,
        valeur,
        id_imputation: imputation.id,
        commentaire: imputation.commentaire,
        nom: imputation.user.nom,
        roles: imputation.user.roles,
        codecapit: imputation.user.codecapit,
        codeprojet: codeprojet.libelle,
        tache: tache.libelle,
        activite: activite.libelle,
      }));
    console.log("details");
    console.log(result(this.details));

    if (this.details) {
      this.exportList = result(this.details);
      console.log(this.exportList);
      this.exportExcel()
    }

  }
  ExporterMois() {
    this.recherche = false;
    this.expoSemaine = false;
    this.expoMois = false;
  }
  getWeek(dateStart) {
    this.dateList = [];
    var datePipe = new DatePipe('en-US');
    let date = datePipe.transform(dateStart, 'yyyy-MM-dd');
    console.log(date);
    this.dateList.push(date);
    var dateOut = new Date(date);
    for (let i = 1; i < 5; i++) {
      dateOut.setDate(dateOut.getDate() + 1);
      var datF = datePipe.transform(dateOut, 'yyyy-MM-dd')
      this.dateList.push(datF);
    }
  }

  exportExcel() {
    if (this.exportList.length > 0) {
      import("xlsx").then(xlsx => {
        const worksheet = xlsx.utils.json_to_sheet(this.exportList);
        const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, "ExportExcel");
      });
    }
  }
  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

  exporterImputationParSemaine() {
    let details = this.DetailsImpService.getDetailsImputation(["2022-06-14", "2022-06-15", "2022-06-13", "2022-06-16", "2022-06-17"]);
    var result = arr =>
      arr.map(({ node: { id, date, valeur, imputation, codeprojet, tache, activite } }) => ({
        id,
        date,
        valeur,
        id_imputation: imputation.id,
        commentaire: imputation.commentaire,
        nom: imputation.user.nom,
        roles: imputation.user.roles,
        codecapit: imputation.user.codecapit,
        codeprojet: codeprojet.libelle,
        tache: tache.libelle,
        activite: activite.libelle,
      }));
    this.exportList = result(details);
    console.log(this.exportList);
    this.exportExcel()

  }
  AddValueImputation(formI: NgForm) {
    this.detailsImputationValue[0] = (formI.form.get("lun").value);
    this.detailsImputationValue[1] = (formI.form.get("mar").value);
    this.detailsImputationValue[2] = (formI.form.get("mer").value);
    this.detailsImputationValue[3] = (formI.form.get("jeu").value);
    this.detailsImputationValue[4] = (formI.form.get("ven").value);
    console.log(this.detailsImputationValue);
  }
}

