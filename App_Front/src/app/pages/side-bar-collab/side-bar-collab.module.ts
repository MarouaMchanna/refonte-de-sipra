import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SideBarCollabComponent } from './side-bar-collab.component';



@NgModule({
  declarations: [],
  imports: [RouterModule,
    CommonModule
  ],
  exports: []
})
export class SideBarCollabModule { }
