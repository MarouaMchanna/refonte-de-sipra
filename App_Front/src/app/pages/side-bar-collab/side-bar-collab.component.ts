import { Component, OnInit } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';

declare const $: any;

//Metadata
export interface RouteInfo {
  path: string;
  title: string;
  type: string;
  icontype: string;
  collapse?: string;
  children?: ChildrenItems[];
}

export interface ChildrenItems {
  path: string;
  title: string;
  ab: string;
  type?: string;
}

//Menu Items
export const ROUTES: RouteInfo[] = [{
  path: 'accueil',
  title: 'accueil',
  type: 'link',
  icontype: 'dashboard'
}, {
  path: 'imputation',
  title: 'imputation',
  type: 'link',
  icontype: 'apps'
}
];
@Component({
  selector: 'app-side-bar-collab',
  templateUrl: './side-bar-collab.component.html'
})
export class SideBarCollabComponent implements OnInit {

  public menuItems: any[];
  ps: any;
  nom: any;
  urlImage: string;
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
      const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
      this.ps = new PerfectScrollbar(elemSidebar);
    }
    this.urlImage = "http://localhost:8000/uploads/images/users/" + localStorage.getItem("image");
    this.nom = localStorage.getItem("nom");
    console.log(this.urlImage);
    console.log(this.nom);
  }
  updatePS(): void {
    if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
      this.ps.update();
    }
  }
  isMac(): boolean {
    let bool = false;
    if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
      bool = true;
    }
    return bool;
  }
}
