import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { APP_BASE_HREF } from '@angular/common';
import { CommonModule } from '@angular/common';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';

import { MatPaginatorModule } from '@angular/material/paginator';
import { ApolloModule, Apollo, APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLink } from 'apollo-angular/http';
import { InMemoryCache } from '@apollo/client/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

import { SidebarModule } from './pages/sidebar/sidebar.module';
import { FooterModule } from './pages/shared/footer/footer.module';
import { NavbarModule } from './pages/shared/navbar/navbar.module';
import { FixedpluginModule } from './pages/shared/fixedplugin/fixedplugin.module';
import { AdminLayoutComponent } from './pages/layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './pages/layouts/auth/auth-layout.component';

import { AppRoutes } from './app.routing';
import { RegisterComponent } from './login/register.component';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { ImputationComponent } from './pages/imputation/imputation.component';
import { CollaborateursComponent } from './pages/collaborateurs/Collaborateurs/collaborateurs.component';
import { AjoutCollabComponent } from './pages/collaborateurs/ajout-collab/ajout-collab.component';
import { CodeProjetComponent } from './pages/projets/codeProjet/code-projet/code-projet.component';
import { AjoutCodeProjetComponent } from './pages/projets/codeProjet/ajout-code-projet/ajout-code-projet.component';
import { TachesComponent } from './pages/projets/tache/taches/taches.component';
import { AjoutTacheComponent } from './pages/projets/tache/ajout-tache/ajout-tache.component';
import { ActivitesComponent } from './pages/projets/activite/activites/activites.component';
import { AjoutActiviteComponent } from './pages/projets/activite/ajout-activite/ajout-activite.component';
import { DetailsProjetComponent } from './pages/projets/projet/details-projet/details-projet.component';
import { AjoutProjetComponent } from './pages/projets/projet/ajout-projet/ajout-projet.component';
import { SideBarCollabComponent } from './pages/side-bar-collab/side-bar-collab.component';
import { SideBarCollabModule } from './pages/side-bar-collab/side-bar-collab.module';
import { DetailsProfilComponent } from './pages/profil/details-profil/details-profil.component';
import { ModifierProfilComponent } from './pages/profil/modifier-profil/modifier-profil.component';
import { UpdateCollabComponent } from './pages/collaborateurs/update-collab/update-collab.component';
import { UpdateProjetComponent } from './pages/projets/projet/update-projet/update-projet.component';
import { UpdateCodeProjetComponent } from './pages/projets/codeProjet/update-code-projet/update-code-projet.component';
import { UpdateActiviteComponent } from './pages/projets/activite/update-activite/update-activite.component';


@NgModule({
  exports: [
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatCardModule,
    MatChipsModule,
    MatCheckboxModule,
    MatStepperModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatDatepickerModule,
    MatButtonModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule



  ],

  imports: [],

  declarations: [
  ]
})
export class MaterialModule { }

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    FormsModule,
    RouterModule.forRoot(AppRoutes, {
      useHash: true
    }),
    HttpClientModule,
    MaterialModule,
    SidebarModule,
    SideBarCollabModule,
    NavbarModule,
    FooterModule,
    FixedpluginModule,
    ApolloModule,
    HttpClientModule
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    RegisterComponent,
    AccueilComponent,
    ImputationComponent,
    CollaborateursComponent,
    AjoutCollabComponent,
    CodeProjetComponent,
    AjoutCodeProjetComponent,
    TachesComponent,
    AjoutTacheComponent,
    ActivitesComponent,
    AjoutActiviteComponent,
    DetailsProjetComponent,
    AjoutProjetComponent,
    SideBarCollabComponent,
    UpdateCollabComponent,
    DetailsProfilComponent,
    ModifierProfilComponent,
    UpdateProjetComponent,
    UpdateCodeProjetComponent,
    UpdateActiviteComponent


  ],
  providers: [
    MatNativeDateModule,
    Permissions,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(apollo: Apollo, httpLink: HttpLink) {
    apollo.create({
      link: httpLink.create({ uri: 'http://localhost:8000/api/graphql' }),
      cache: new InMemoryCache()
    })
  }
}
