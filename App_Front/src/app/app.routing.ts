import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './pages/layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './pages/layouts/auth/auth-layout.component';
import { RegisterComponent } from './login/register.component';
import { AjoutCollabComponent } from './pages/collaborateurs/ajout-collab/ajout-collab.component';
import { CodeProjetComponent } from './pages/projets/codeProjet/code-projet/code-projet.component';
import { AjoutCodeProjetComponent } from './pages/projets/codeProjet/ajout-code-projet/ajout-code-projet.component';
import { TachesComponent } from './pages/projets/tache/taches/taches.component';
import { AjoutTacheComponent } from './pages/projets/tache/ajout-tache/ajout-tache.component';
import { AjoutActiviteComponent } from './pages/projets/activite/ajout-activite/ajout-activite.component';
import { ActivitesComponent } from './pages/projets/activite/activites/activites.component';
import { DetailsProjetComponent } from './pages/projets/projet/details-projet/details-projet.component';
import { AjoutProjetComponent } from './pages/projets/projet/ajout-projet/ajout-projet.component';
import { CollaborateursComponent } from './pages/collaborateurs/Collaborateurs/collaborateurs.component';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { ImputationComponent } from './pages/imputation/imputation.component';
import { DetailsProfilComponent } from './pages/profil/details-profil/details-profil.component';
import { ModifierProfilComponent } from './pages/profil/modifier-profil/modifier-profil.component';
import { UpdateCollabComponent } from './pages/collaborateurs/update-collab/update-collab.component';
import { UpdateProjetComponent } from './pages/projets/projet/update-projet/update-projet.component';
import { UpdateCodeProjetComponent } from './pages/projets/codeProjet/update-code-projet/update-code-projet.component';
import { UpdateActiviteComponent } from './pages/projets/activite/update-activite/update-activite.component';
import { IsSignedInGuard } from './services/IsSignedIn/IsSignedIn.guard';


export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    },

    {
        path: 'login',
        component: RegisterComponent,
    },
    {
        path: 'Admin',
        component: AdminLayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'accueil',
                pathMatch: 'full',
            },
            {
                path: 'accueil',
                component: AccueilComponent,
            }, {
                path: 'profil',
                component: DetailsProfilComponent,
            }, {
                path: 'ModifierProfil',
                component: ModifierProfilComponent,
            }, {
                path: 'imputation',
                component: ImputationComponent,
            }, {
                path: 'projets/details',
                component: DetailsProjetComponent
            }, {
                path: 'projets/ajout-projet',
                component: AjoutProjetComponent
            }, {
                path: 'projets/codeProjet',
                component: CodeProjetComponent,
            }, {
                path: 'projets/ajout-codeprojet',
                component: AjoutCodeProjetComponent
            }, {
                path: 'projets/taches',
                component: TachesComponent
            }, {
                path: 'projets/ajout-tache',
                component: AjoutTacheComponent
            }, {
                path: 'projets/activites',
                component: ActivitesComponent
            }, {
                path: 'projets/ajout-activite',
                component: AjoutActiviteComponent
            }, {
                path: 'projets/activites/update-activite',
                component: UpdateActiviteComponent
            }, {
                path: 'Collaborateurs',
                component: CollaborateursComponent
            },
            {
                path: 'collaborateurs/Ajout',
                component: AjoutCollabComponent
            },
            {
                path: 'Collaborateurs/Update',
                component: UpdateCollabComponent
            },
            {
                path: 'projets/details/update-projet',
                component: UpdateProjetComponent
            },
            {
                path: 'projets/codeProjet/update-code-projet',
                component: UpdateCodeProjetComponent
            }


        ]
    }, {
        path: 'User',
        component: AuthLayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'accueil',
                pathMatch: 'full',
            },
            {
                path: 'accueil',
                component: AccueilComponent,
            }, {
                path: 'imputation',
                component: ImputationComponent,
            }, {
                path: 'profil',
                component: DetailsProfilComponent,
            }, {
                path: 'ModifierProfil',
                component: ModifierProfilComponent,
            }]
    }
];
