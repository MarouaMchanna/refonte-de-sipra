import { Injectable } from '@angular/core';
import { ApiService } from '../api-service/api.service';
import { Apollo } from 'apollo-angular';
import { gql } from 'graphql-tag';
import { imputation } from 'src/app/models/imputation';
import { identity } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ImputationService {
  imputations: any[];
  loading = true;
  error: any;
  id: any;
  constructor(private api: ApiService, private apollo: Apollo) { }

  // récuperer imputation par user et date
  getImputation(date: String[], user: String) {

    this.apollo.watchQuery({
      query: gql`
      query($date_list: [String],$user:String)  {
        imputations(user:$user){
    			 edges{
            node{
              id
              commentaire
              user{
                id
              }
              detailsImputations(date_list:$date_list){
                edges{
            node{
              id
              valeur
              codeprojet{
                id
                libelle
              }
              tache{
                id
                libelle
              }
              activite{
                id
                libelle
              }
            }}
              }
            }
          }
        }
      }
    `,
      variables: {
        user: user,
        date_list: date
      }
    })
      .valueChanges.subscribe((result: any) => {
        this.imputations = result?.data?.imputations.edges;
        this.loading = result.loading;
        this.error = result.error;


      });
    console.log("imputation service,get Imp by user & date " + this.imputations);
    return this.imputations;

  }
  // Inserer imputation
  AjouterImputation(imputation: imputation): String {

    const mutationAjout = gql` mutation CreateImputation(
        $commentaire : String! , $user: String! )
      {
        createImputation(input: {commentaire : $commentaire, user:$user,
      }){
        imputation{
          id
        }
      }
      }
     ,`;
    this.apollo.mutate({
      mutation: mutationAjout,
      variables: {
        commentaire: imputation.commentaire,
        user: imputation.user_id,
      }
    }).subscribe((result: any) => {
      this.id = result?.data?.createImputation.imputation.id;
      this.loading = result.loading;
      this.error = result.error;


    });
    console.log("Ajouter, imputation service");
    return this.id;
  }




  ///////////////////// test
  //Ajouter imputation
  postImputation(data: any) {
    this.api.postImp(data).subscribe(
      (result) => {
        console.log("imputation a été ajoutée avec succes ");
      }, (error) => {
        console.log("erreur : imputation non ajoutée ");
      });

  }
}
