import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
   baseUrl = 'http://localhost:8000/api/'; 

  constructor(private http: HttpClient) { }

  //code projet 
  getCP():Observable<any>{
    return this.http.get(this.baseUrl+`code_projets`);
  }

  //activite
  getact():Observable<any>{
    return this.http.get(this.baseUrl+`activites`);
  }

  //tache
  getTc():Observable<any>{
    return this.http.get(this.baseUrl+`taches`);
  }

  //imputation
  postImp(data: any){
    return this.http.post(this.baseUrl+`imputations`,data);
  }

  //details imputation
  postDetImp(data : any){
    return this.http.post(this.baseUrl+'details_imputations',data);
  }
}
