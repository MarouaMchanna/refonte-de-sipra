import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { projet } from 'src/app/models/projet';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root'
})
export class ProjetService {
  projets: projet[];
  loading = true;
  error: any;
  projet: projet;
  constructor(private apollo: Apollo) { }

  //Afficher tous les projets
  AfficherProjets() {
    this.apollo.watchQuery({
      query: gql` 
      { projets{
          edges{
            node{
              id
              statut
              libelle
              description
            }
          }
        }
      }
      `,
    }).valueChanges.subscribe((result: any) => {
      this.projets = result?.data?.projets.edges;
      this.loading = result.loading;
      this.error = result.error;
    });
    console.log("Affichage projets");
    console.log(this.projets);
    return this.projets;
  }

  //afficher un projet par rapport a son id
  getProjet(id: any) {
    this.error = "";
    this.loading = true;
    this.apollo
      .query<any>({
        query: gql`
          query($id: ID!) {
            projet(id: $id) {
              id
              libelle
              description
              statut
            }
          }
        `,
        variables: {
          id: id
        }
      })
      .subscribe(({ data, loading }) => {
        if (data.projet) this.projet = data.projet;
        else this.error = "projet does not exits";
        this.loading = loading;
      });
    return this.projet
  }

  //Ajouter un projet
  AjouterProjet(projet: projet) {
    const mutationAjout = gql` mutation CreateProjet(
    $libelle: String!,$description: String!,$statut: Boolean!)
    {
      createProjet(input: {libelle :$libelle, description:$description,statut:$statut
    }){
      projet{
        id
      }
    }
    }
   ,`;
    this.apollo.mutate({
      mutation: mutationAjout,
      variables: {
        libelle: projet.libelle, description: projet.description, statut: projet.statut
      }
    }).subscribe();
    console.log("Ajouter, projet service");
  }

  //Update un projet
  UpdateProjet(projet: projet) {
    const mutationUpdate = gql` mutation  UpdateProjet($id: ID!,
      $libelle: String!,$description: String!,$statut: Boolean!)
      {
        updateProjet(input: {id:$id, libelle :$libelle, description:$description,statut:$statut
      }){
        projet{
          id
        }
      }
      }
     ,`;
    this.apollo.mutate({
      mutation: mutationUpdate,
      variables: {
        id: projet.id, libelle: projet.libelle, description: projet.description, statut: projet.statut
      }
    }).subscribe();
    console.log("update, projet service");
  }

  //Supprimer un projet
  SupprimerProjet(idProjet: string) {
    const mutationSupp = gql` 
    mutation DeleteProjet($id: ID!)
    {
      DeleteProjet(input: {id :$id}){
        projet{
          id
        }
      }
    }
     ,`;
    this.apollo.mutate({
      mutation: mutationSupp,
      variables: {
        id: idProjet,
      }
    }).subscribe();
    console.log("Supprimer, projet service");

  }

}
