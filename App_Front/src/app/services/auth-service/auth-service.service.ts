import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(private http: HttpClient) { }

  login(data: any): Observable<any> {
    // console.log("I am server");
    return this.http.post(`http://localhost:8000/api/login_check`, data);
  }

  logout(): Observable<any> {
    console.log("User Logout ");
    return this.http.get(`http://localhost:8000/api/logout`);
  }

  GetUserAuth(data: any): Observable<any> {
    return this.http.post(`http://localhost:8000/api/decoderToken`, data);
  }

  isLogged(): boolean {
    if (localStorage.getItem("isSigned") == "true")
      return true;
    else
      return false;
  }
}
