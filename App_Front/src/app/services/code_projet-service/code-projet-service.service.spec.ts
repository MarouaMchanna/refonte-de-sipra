import { TestBed } from '@angular/core/testing';

import { CodeProjetServiceService } from './code-projet-service.service';

describe('CodeProjetServiceService', () => {
  let service: CodeProjetServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CodeProjetServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
