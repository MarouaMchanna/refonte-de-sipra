import { Injectable } from '@angular/core';
import { codeProjet } from 'src/app/models/codeProjet';
import { plainToClass } from 'class-transformer';
import { Apollo, gql } from 'apollo-angular';
@Injectable({
  providedIn: 'root'
})
export class CodeProjetServiceService {
  codeProjets: codeProjet[];
  codeProet: codeProjet;
  loading = true;
  error: any;
  constructor(private apollo: Apollo) { }

  // liste des codes projets
  getCodeProjet(): codeProjet[] {
    this.apollo.watchQuery({
      query: gql` 
    {
     codeProjets{
     edges{
      node{
        _id
        id
        libelle
        description
        statut
        budget
        dateFin
        dateDebut
        charge
        budgetCLOEconsomme
        budgetDECOconsomme
        budgetNRJconsomme
        budgetCLOE
        budgetDECO
        budgetNRJ
        chargeCLOE
        chargeDECO
        chargeNRJ
        chargeNRJconsomme
        chargeCLOEconsomme
        chargeNRJconsomme
        taches{
          edges{
            node{
              id
              libelle
            }
          }
          
         }
       }
     }
     }
    }
      `,
    }).valueChanges.subscribe((result: any) => {
      this.codeProjets = result?.data?.codeProjets.edges;
      this.loading = result.loading;
      this.error = result.error;
    });
    console.log("Affichage projets");
    console.log(this.codeProjets);
    return this.codeProjets;
  }

  //Afficher un code projet par rapport a son id
  getCProjet(id: any): codeProjet {
    this.apollo
      .query<any>({
        query: gql`
          query($id: ID!) {
            codeProjet(id: $id) {
             id,
             libelle ,
             description,
             statut,
             budget,
             dateFin,
             dateDebut,
             charge,
             budgetNRJ,
             budgetDECO,
             budgetCLOE,
             chargeNRJ,
             chargeDECO,
             chargeCLOE,
             chargeNRJconsomme,
             chargeDECOconsomme,
             chargeCLOEconsomme,
             budgetNRJconsomme,
             budgetDECOconsomme,
             budgetCLOEconsomme,
            }
          }
        `,
        variables: {
          id: id
        }
      })
      .subscribe(({ data, loading }) => {
        if (data.codeProjet) this.codeProet = data.codeProjet;
        else this.error = "code projet does not exits";
        this.loading = loading;
      });
    console.log(this.codeProet);
    return this.codeProet;

  }

  //Ajouter un code projet
  AjouterCodeProjet(Cprojet: codeProjet) {
    const mutationAjout = gql` mutation CreateCodeProjet(
    $libelle: String!,$description: String!,$statut: Boolean!,$budget:String!,$dateFin:String!,$dateDebut:String!,
    $charge:String!,$budgetNRJ:String!,$budgetCLOE:String!,$budgetDECO:String!,$chargeCLOE:String!,$chargeDECO:String!,$chargeNRJ:String!)
    {
      createCodeProjet(input:{
      libelle:$libelle ,
      description:$description,
      statut:$statut,
      budget:$budget,
      dateFin:$dateFin,
      dateDebut:$dateDebut,
      charge:$charge,
      budgetNRJ:$budgetNRJ,
      budgetDECO:$budgetDECO,
      budgetCLOE:$budgetCLOE,
      chargeNRJ:$chargeNRJ,
      chargeDECO:$chargeDECO,
      chargeCLOE:$chargeCLOE,
      chargeNRJconsomme:"",
      chargeDECOconsomme:"",
      chargeCLOEconsomme:"",
      budgetNRJconsomme:"",
      budgetDECOconsomme:"",
      budgetCLOEconsomme:""
    }){
      codeProjet{
        id
      }
    }
    }
   ,`;
    this.apollo.mutate({
      mutation: mutationAjout,
      variables: {
        libelle: Cprojet.libelle, description: Cprojet.description, statut: Cprojet.statut, budget: Cprojet.budget,
        dateFin: Cprojet.dateFin, dateDebut: Cprojet.dateDebut, charge: Cprojet.charge, budgetNRJ: Cprojet.budgetNRJ,
        budgetCLOE: Cprojet.budgetCLOE, budgetDECO: Cprojet.budgetDECO, chargeCLOE: Cprojet.chargeCLOE, chargeDECO: Cprojet.chargeDECO,
        chargeNRJ: Cprojet.chargeNRJ,
      }

    }).subscribe();
    console.log("Ajouter, code projet service");
  }

  //Modifier un code projet
  UpdateCodeProjet(Cprojet: codeProjet) {
    const mutationAjout = gql` mutation UpdateCodeProjet(
    $id: ID!,$libelle: String!,$description: String!,$statut: Boolean!,$budget:String!,$dateFin:String!,$dateDebut:String!,
    $charge:String!,$budgetNRJ:String!,$budgetCLOE:String!,$budgetDECO:String!,$chargeCLOE:String!,$chargeDECO:String!,$chargeNRJ:String!)
    {
      updateCodeProjet(input:{
      id:$id,
      libelle:$libelle ,
      description:$description,
      statut:$statut,
      budget:$budget,
      dateFin:$dateFin,
      dateDebut:$dateDebut,
      charge:$charge,
      budgetNRJ:$budgetNRJ,
      budgetDECO:$budgetDECO,
      budgetCLOE:$budgetCLOE,
      chargeNRJ:$chargeNRJ,
      chargeDECO:$chargeDECO,
      chargeCLOE:$chargeCLOE,
    }){
      codeProjet{
        id
      }
    }
    }
   ,`;
    this.apollo.mutate({
      mutation: mutationAjout,
      variables: {
        id: Cprojet.id, libelle: Cprojet.libelle, description: Cprojet.description, statut: Cprojet.statut, budget: Cprojet.budget,
        dateFin: Cprojet.dateFin, dateDebut: Cprojet.dateDebut, charge: Cprojet.charge, budgetNRJ: Cprojet.budgetNRJ,
        budgetCLOE: Cprojet.budgetCLOE, budgetDECO: Cprojet.budgetDECO, chargeCLOE: Cprojet.chargeCLOE, chargeDECO: Cprojet.chargeDECO,
        chargeNRJ: Cprojet.chargeNRJ,
      }

    }).subscribe();
    console.log("Modifier, code projet service");
  }


  //Supprimer un code projet
  SupprimerCProjet(id: string) {
    const mutationSupp = gql` 
    mutation DeleteCodeProjet($id: ID!)
    {
      deleteCodeProjet(input: {id :$id}){
        codeProjet{
          id
        }
      }
    }
     ,`;
    this.apollo.mutate({
      mutation: mutationSupp,
      variables: {
        id: id,
      }
    }).subscribe();
    console.log("Supprimer, code projet service");

  }
}
