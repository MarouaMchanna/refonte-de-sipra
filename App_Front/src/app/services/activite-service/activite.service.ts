import { Injectable } from '@angular/core';
import { activite } from 'src/app/models/activite';
import { plainToClass } from 'class-transformer';
import { Apollo, gql } from 'apollo-angular';

@Injectable({
  providedIn: 'root'
})
export class ActiviteService {
  private activites: activite[];
  loading = true;
  error: any;
  activite: activite;

  constructor(private apollo: Apollo) { }

  //liste des activites
  getActivite() {
    return this.apollo.watchQuery({
      query: gql` 
      {
         activites{
           edges{
           node{
             _id
             id
             libelle
             description
       }
    }
  }
}
      `,
    })
    console.log("Affichage projets");
    console.log(this.activites);
    //return this.activites;
  }


  //afficher une activite par rapport a son id
  getActivit(id: any) {
    this.error = "";
    this.loading = true;
    this.apollo
      .query<any>({
        query: gql`
          query($id: ID!) {
            activite(id: $id) {
              id
              libelle
              description
            }
          }
        `,
        variables: {
          id: id
        }
      })
      .subscribe(({ data, loading }) => {
        if (data.activite) this.activite = data.activite;
        else this.error = "activite does not exits";
        this.loading = loading;
      });
    return this.activite;
  }

  //Ajouter une activite
  AjouterActivite(activite: activite) {
    const mutationAjout = gql` mutation CreateActivite(
    $libelle: String!,$description: String!)
    {
      createActivite(input: {libelle :$libelle, description:$description
    }){
      activite{
        id
      } 
    }
    }
   ,`;
    this.apollo.mutate({
      mutation: mutationAjout,
      variables: {
        libelle: activite.libelle, description: activite.description
      }
    }).subscribe();
    console.log("Ajouter, activite service");
  }

  //Update une activite
  UpdateActivite(activite: activite) {
    const mutationUpdate = gql` mutation  UpdateActivite($id: ID!,
      $libelle: String!,$description: String!)
      {
        updateActivite(input: {id:$id, libelle :$libelle, description:$description
      }){
        activite{
          id
        }
      }
      }
     ,`;
    this.apollo.mutate({
      mutation: mutationUpdate,
      variables: {
        id: activite.id, libelle: activite.libelle, description: activite.description
      }
    }).subscribe();
    console.log("update, activite service");
  }
}
