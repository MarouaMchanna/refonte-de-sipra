import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { user } from 'src/app/models/user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { gql } from 'graphql-tag';

@Injectable({
  providedIn: 'root'
})
export class CollaborateurService {
  users: user[];
  usersT: user[];
  userProfil: user[];
  user: user;
  loading = true;
  error: any;


  constructor(private apollo: Apollo, private http: HttpClient) { }

  getCollabs() {
    this.apollo.query<any>({
      query: gql`
      {
        users{
          edges{
            node{
              id
              codecapit
              nom
              email
              poste
              roles
              projet{
                id
                libelle
              }
            }
          }
        }
      }
    `,
    }).subscribe((result: any) => {
      this.usersT = result?.data?.users.edges;
      this.loading = result.loading;
      this.error = result.error;
    });
    console.log("this.users");
    console.log(this.users);
    return this.usersT;
  }
  ////////////////////////
  getColla() {
    return this.apollo.query<any>({
      query: gql`
     {
       users{
         edges{
           node{
             id
             codecapit
             nom
             email
             poste
             roles
             projet{
               id
               libelle
             }
           }
         }
       }
     }
   `,
    });
  }
  //////////////////////
  getCollab(id: any) {
    this.error = "";
    this.loading = true;
    this.apollo
      .query<any>({
        query: gql`
          query($id: ID!) {
            user(id: $id) {
              nom
              codecapit
              image
              email
              site
              poste
              password
              CJM
              roles
              projet {
                id
                libelle
              }
            }
          }
        `,
        variables: {
          id: id
        }
      })
      .subscribe(({ data, loading }) => {
        if (data.user) this.user = data.user;
        else this.error = "user does not exits";
        this.loading = loading;
      });
    return this.user
  }





  //Ajouter un collaborateur
  AjouterCollab(user: user) {
    const mutationAjout = gql` mutation CreateCollaborateur(
    $email: String!,$roles: Iterable!,$password: String!,$poste: String!,$projet: String,
    $CJM: String!,$codecapit: String!,$site: String!,$image: String!,$nom: String,$cJM: String!)
    {
      createUser(input: {email :$email, roles:$roles,password:$password,nom:$nom,
        poste:$poste,image:$image,CJM:$CJM,cJM:$cJM,codecapit:$codecapit,site:$site,projet:$projet
    }){
      user{
        id
      }
    }
    }
   ,`;
    this.apollo.mutate({
      mutation: mutationAjout,
      variables: {
        email: user.email, roles: user.roles, password: user.password, nom: user.nom,
        poste: user.poste, image: user.image, CJM: user.CJM, cJM: user.CJM, codecapit: user.codecapit, site: user.site,
        projet: user.projet
      }
    }).subscribe(({ data }) => {
      console.log('got data', data);
    }, (error) => {
      console.log('there was an error sending the query', error);
    });
    console.log("Ajouter, Collab service");



  }
  //Ajouter image
  AjouterImage(data: any): Observable<any> {
    console.log("I am server http://localhost:8000/api/saveImage");
    return this.http.post(`http://localhost:8000/api/saveImage`, data);
  }
  //Supprimer un collaborateur
  SupprimerCollab(idUser: string) {
    const mutationAjout = gql` 
    mutation DeleteCollaborateur($id: ID!)
    {
      deleteUser(input: {id :$id}){
        user{
          id
        }
      }
    }
     ,`;
    this.apollo.mutate({
      mutation: mutationAjout,
      variables: {
        id: idUser,
      }
    }).subscribe();
    console.log("Supprimer, Collab service");

  }
  //modifier les info perso 
  modifierInfoP(userM: user) {
    const mutationModif = gql`
    mutation modifCollab($id: ID!,$email: String,$site: String,$image: String){
      updateUser(input: {id :$id,site:$site,email:$email,image:$image}){
        user{
          id
        }
      }
    }
  `;
    this.apollo.mutate({
      mutation: mutationModif,
      variables: {
        id: userM.id,
        email: userM.email,
        site: userM.site,
        image: userM.image,
      }
    }).subscribe();
    console.log("Modifier, Collab service");
  }
  //modifier le mdp
  modifiermdp(id: String, password: String) {
    const mutationModif = gql`
    mutation modifCollab($id: ID!,$password: String){
      updateUser(input: {id :$id,password:$password}){
        user{
          id
        }
      }
    }
  `;
    this.apollo.mutate({
      mutation: mutationModif,
      variables: {
        id: id,
        password: password,
      }
    }).subscribe();
    console.log("Modifier, Collab service");
  }
  // récuperer collaborateur by email
  getCollabByEmail(email: any) {
    this.apollo.watchQuery({
      query: gql`
      query($email: String!) {
        users(email:$email){
          edges{
            node{
              id
              codecapit
              nom
              email
              poste
              roles
              password
              site
              CJM
              image
              projet{
                id
                libelle
              }
            }
          }
        }
      }
    `,
      variables: {
        email: email
      }
    }).valueChanges.subscribe((result: any) => {
      this.userProfil = result?.data?.users.edges;
      this.loading = result.loading;
      this.error = result.error;
    });
    console.log("this.users test email" + this.userProfil);
    return this.userProfil;

  }
  ///////////////////// 
  getCollabByE(email: any) {

    return this.apollo.query<any>({
      query: gql`
      query($email: String!) {
        users(email:$email){
          edges{
            node{
              id
              codecapit
              nom
              email
              poste
              roles
              password
              site
              CJM
              image
              projet{
                id
                libelle
              }
            }
          }
        }
      }
    `,
      variables: {
        email: email
      }
    });

  }
  ////////////////////

  // recuperer collaborateur by id
  getCollab2(id: any) {
  }
  //................. Partie Test...................................
  //methode test
  test() {
    this.apollo.watchQuery({
      query: gql`
      {
        users{
          edges{
            node{
              id
            }
          }
        }
      }
    `,
    }).valueChanges.subscribe((result: any) => {
      this.users = result?.data?.users;
      this.loading = result.loading;
      this.error = result.error;
    });
    console.log("this.users");
    console.log(this.users);
  }
  // Ajouter activité test
  AjoutActivite(lib, des) {
    const mutationTest = gql` mutation CreateActivites($libelle: String!,$description: String!)
    {
      createActivites(input: {libelle:$libelle ,description : $description
      }){
        activites{
          id
        }
      }
    }
   ,`;
    this.apollo.mutate({
      mutation: mutationTest,
      variables: {
        libelle: lib,
        description: des
      }

    }).subscribe();
    console.log("Ajouter Activite service");
  }


  //Update un collaborateur
  UpdateCollab(user: user) {
    const mutationUpdate = gql` mutation UpdateCollaborateur($id: ID!
    $email: String!,$roles: Iterable!,$password: String!,$poste: String!,$projet: String,
    $CJM: String!,$codecapit: String!,$site: String!,$image: String!,$nom: String,$cJM: String!)
    {
      updateUser(input: {id:$id, email :$email, roles:$roles,password:$password,nom:$nom,
        poste:$poste,image:$image,CJM:$CJM,cJM:$cJM,codecapit:$codecapit,site:$site,projet:$projet
    }){
      user{
        id
      }
    }
    }
   ,`;
    this.apollo.mutate({
      mutation: mutationUpdate,
      variables: {
        id: user.id,
        email: user.email, roles: user.roles, password: user.password, nom: user.nom,
        poste: user.poste, image: user.image, CJM: user.CJM, cJM: user.CJM, codecapit: user.codecapit, site: user.site,
        projet: user.projet
      }
    }).subscribe();
    console.log("Update Collab service");
  }

}
