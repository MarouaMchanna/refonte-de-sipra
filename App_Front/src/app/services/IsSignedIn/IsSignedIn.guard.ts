import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import { AuthServiceService } from "../auth-service/auth-service.service";

class UserToken { }
class Permissions {
    canActivate(user: UserToken, id: string): boolean {
        return true;
    }
}

@Injectable({
    providedIn: 'root'
})
export class IsSignedInGuard implements CanActivate {

    constructor(private _router: Router, private _auth: AuthServiceService) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        if (this._auth.isLogged() == true) {
            if (localStorage.getItem("roles").indexOf("ROLE_ADMIN") > -1) {
                //this._router.navigate(['/Admin']);
            }
            else {
                //this._router.navigate(['/User']);
            }
            return true;
        }
        else {

            this._router.navigate(['/login']);
            return false;

        }


    }
}