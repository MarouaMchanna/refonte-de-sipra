import { Injectable } from '@angular/core';
import { ApiService } from '../api-service/api.service';
import { Apollo } from 'apollo-angular';
import { gql } from 'graphql-tag';
import { detailsImputation } from 'src/app/models/detailsImputation';

@Injectable({
  providedIn: 'root'
})
export class DetailsImputationService {
  detailsImputation: any[];
  detailsImputationT: any[];
  detailsImputationF: any[];
  loading = true;
  error: any

  constructor(private apollo: Apollo, private api: ApiService) { }



  // récuperer details imputation par date
  getDetailsImputation(date: String[]) {

    this.apollo.watchQuery({
      query: gql`
      query($date_list: [String]) {
        detailsImputations(date_list:$date_list){
          edges{
            node{
              id
           		valeur
              date
              imputation{
                id
                commentaire
                user{
                  nom
                  roles
                  codecapit
                }
              }
              codeprojet{
                id
                libelle
                dateDebut
                dateFin
                description
                budget
                charge
              }
              tache{
                id
                libelle
								description
              }
              activite{
                id
                libelle
                description
              }
            }
          }
        }
      }
    `,
      variables: {
        date_list: date
      }
    }).valueChanges.subscribe((result: any) => {
      this.detailsImputation = result?.data?.detailsImputations.edges;
      this.loading = result.loading;
      this.error = result.error;


    });
    console.log("details-imputation service,get detailsImp by date " + this.detailsImputation);
    return this.detailsImputation;

  }
  ChangeObject(arr: any) {

  }
  // recuperer imputation par semaine
  getDetailsImputationParSemaine() {
    /*   var result = arr =>
       arr.map(({ node: { id, date,valeur, imputation,codeprojet,tache,activite} }) => ({
         id,
         date,
         valeur,
         id_imputation:imputation.id,
         commentaire: imputation.commentaire,
         nom:imputation.user.nom,
         roles:imputation.user.roles,
         codecapit:imputation.user.codecapit,
         codeprojet:codeprojet.libelle,
         tache:tache.libelle,
         activite:activite.libelle,
       }));
   
       let transformedJSON = result(this.getDetailsImputation("2022-06-14"));
       let transformedJSON2= result(this.getDetailsImputation("2022-06-20"));
       this.detailsImputationF=transformedJSON.concat(transformedJSON2);
       console.log(this.detailsImputationF);
       
    */

  }

  //////////////////////////////// test 
  // récuperer details imputation par date
  getTestDT(date: String) {
    this.apollo.watchQuery({
      query: gql`
      query($date: String!) {
        detailsImputations(date:$date){
          edges{
            node{
              id
           		valeur
              date
              imputation{
                id
                commentaire
              }
            }
          }
        }
      }
    `,
      variables: {
        date: date
      }
    }).valueChanges.subscribe((result: any) => {
      this.detailsImputationT = result?.data?.detailsImputations.edges;
      this.loading = result.loading;
      this.error = result.error;
    });
    console.log("details-imputation service,get detailsImp by date ");
    return this.detailsImputationT;

  }

  // Inserer imputation
  AjouterImputation(imputation: detailsImputation) {

    const mutationAjout = gql` mutation CreateImputation(
      $date : String! , $valeur: String!, $codeprojet_id: String!, $tache_id: String!, $activite_id: String!, $imputation_id: String! )
    {
      createDetailsImputation(input:{date:$date, valeur:$valeur, codeprojet:$codeprojet_id,tache:$tache_id,activite: $activite_id, imputation:$imputation_id}){
  detailsImputation{
    id
  }
}
    }
   ,`;
    this.apollo.mutate({
      mutation: mutationAjout,
      variables: {
        date: imputation.date,
        valeur: imputation.valeur,
        codeprojet_id: imputation.codeprojet_id,
        tache_id: imputation.tache_id,
        activite_id: imputation.activite_id,
        imputation_id: imputation.imputation_id,
        //commentaire: imputation.commentaire,
        // user: imputation.user_id,
      }
    }).subscribe();
    console.log("Ajouter, details imputation service");
  }




  //Ajouter details imputation
  postDetailsImputation(data: any) {
    this.api.postDetImp(data).subscribe(
      (result) => {
        console.log("details imputation a été ajouté avec succes ");
      },
      (error) => {
        console.log("erreur : details imputation non ajouté ");
      });
  }
}
