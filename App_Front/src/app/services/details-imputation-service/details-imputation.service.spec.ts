import { TestBed } from '@angular/core/testing';

import { DetailsImputationService } from './details-imputation.service';

describe('DetailsImputationService', () => {
  let service: DetailsImputationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DetailsImputationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
