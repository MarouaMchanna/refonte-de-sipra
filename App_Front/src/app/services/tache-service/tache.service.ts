import { Injectable } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import { tache } from 'src/app/models/tache';

@Injectable({
  providedIn: 'root'
})
export class TacheService {
  taches: tache[];
  loading = true;
  error: any;
  constructor(private apollo: Apollo) { }

  // liste des taches
  getTache() {
    this.apollo.watchQuery({
      query: gql` 
    {
     taches{
       edges{
         node{
           _id
           id
           libelle
           description
           domaine
           codeprojet{
             id
             libelle
             }
          }
        }
      }
    }
      `,
    }).valueChanges.subscribe((result: any) => {
      this.taches = result?.data?.taches.edges;
      this.loading = result.loading;
      this.error = result.error;
    });
    console.log("Affichage projets");
    console.log(this.taches);
    return this.taches;
  }

  //Ajouter une tache
  AjouterTache(tache: tache) {
    const mutationAjout = gql` mutation CreateTache(
      $libelle: String!,$description: String!,$codeprojet: String!,$domaine: String! )
      {
        createTache(input: {libelle :$libelle, description:$description, codeprojet:$codeprojet,domaine:$domaine
      }){
        tache{
          id
        } 
      }
      }
     ,`;
    this.apollo.mutate({
      mutation: mutationAjout,
      variables: {
        libelle: tache.libelle, description: tache.description, codeprojet: tache.codeprojet_id, domaine: tache.domaine
      }
    }).subscribe();
    console.log("Ajouter, tache service");
  }
}

